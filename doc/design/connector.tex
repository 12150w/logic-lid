\subsection{Device Connector}\label{sec:designDeviceConnector}
The device connector allows logic devices to be connected to the base board (and controller).
The device connector must do the following:

\begin{itemize}
	\item \textbf{Multi-Directional}: the device must be allowed to connect to the base board in the up, down, left, and right orientation.
	\item \textbf{Power}: the device is powered through the connector from a power supply on the base board.
	\item \textbf{Orientation}: the device must be capable of determining which orientation it is connected to the device connector (either up, down, left, or right).
	\item \textbf{Location}: the device must be capable of determining which location it is connected at, on the base board, from the device connector.
	\item \textbf{Communication}: the device must be able to communicate with the controller and other devices through the device connector.
\end{itemize}

To achieve these tasks the device connector is made up of an array of 9 pins in a 3-by-3 square.
Figure \ref{fig:connectorRender} is a picture of what this pin array looks like.
Note that because the pins are cheaper than the socket the pins are located in the base board and the socket is located on the devices.

\begin{figure}[ht]
	\centering
	\begin{subfigure}{0.35\textwidth}
		\centering
		\includegraphics[width=\textwidth]{design/images/conn-male.png}
		\caption{Device Connector Pins}
	\end{subfigure}
	\begin{subfigure}{0.35\textwidth}
		\centering
		\includegraphics[scale=0.23]{design/images/conn-sock.png}
		\caption{Device Connector Socket}
	\end{subfigure}
	\caption{Device Connector\label{fig:connectorRender}}
\end{figure}

\subsubsection{Connector Pinout}
Figure \ref{fig:connPinout} shows the pinout of the device connector.
The base board connector pinout never changes and is the reference.
The device connector pinout labels never change but because the connector is multi-directional the actual function of the pins may change (depending on the orientation).
The normal pinout is the typical configuration used when connecting devices.
The programming pinout is only used when programming a device, it is \textbf{not multi-directional}, and there is only 1 special connector on the base board that can program (and can not be used for normal operation).
\textit{Keep in mind that the device connector is on the bottom of the device PCB so the pinout is mirrored from what is shown here.}

\begin{figure}[ht]
	\centering
	\includegraphics[scale=0.5]{design/figures/conn-pinout.pdf}
	\caption{Device Connector Pinout \label{fig:connPinout}}
\end{figure}


\subsubsection{Connector Power}
The middle pin is always ground regardless of the device orientation.
The base board connector provides power (+5V) on one of the 4 corner pins labeled +V.
On the device, any one of the 4 corner pins might be powering the device.
Because the remaining corner pins (\refc, \refl, and \refd) must be used they are isolated from the power pin on the device using diodes.
The device operates at +3.3V and the base board provides +5V so the diode forward voltage drop does not cause the voltage to drop to low.


\subsubsection{Connector Orientation}
Once connected to the base board the device determines its orientation by examining which of the 4 corner pins is high.
On the base board \refc and \refl are pulled low so at any time only +V or \refd and +V are going to be high (+3.3V or +5V) at any time.
The device reads the orientation multiple times until a valid orientation is continually read.
This is done to avoid issues when plugging in the device without all the pins connected at the same time.
Additionally, because the device operates at +3.3V and the +V pin will be at +5V (to compensate for the diode voltage drop) the device microcontroller has current limiting resistors and clamping diodes to prevent the voltage from going above safe operating levels on the microcontroller pins.


\subsubsection{Connector Location}
Once the orientation is known (that is, +V has been located) the remaining corner pins (\refc, \refl, and \refd) are used to communicate with a shift register in the base board to determine the location of the connector.
Each connector on the base board has its own shift register with a different value being shifted in.
The value shifted in also serves as the \itwoc address of the device.
Due to the current limiting resistors on the corner pins, to protect the microcontroller from the +5V on +V, and the pull-down resistors on the base board connector the shift register is driven through NPN transistors which only require about 0.7V to activate.


\subsubsection{Connector Communication}
The remaining 4 side pins are used to communicate with the device using \itwoc.
On the base board connector the SDA (D) and SCL (Clk) pins are connected together so that there are 2 possible orientations of the device.
The device connector has one SDA (D) and one SCL (Clk) pins.
These pins will either be connected to the base board connector correctly (SDA to SDA and SCL to SCL) or reversed (SDA to SCL and SCL to SDA) depending upon the orientation.
Because the device can detect its orientation it will reverse SDA and SCL if its orientation results and SDA and SCL being connected in reverse.


\subsubsection{Connector Programming}
The device may also be programmed through the device connection.
Device programming \textbf{is not multi-directional} and requires that the microcontroller reset be connected to the device connector by toggling a switch on the device.
The base board has a single programming connector that is not connected to the \itwoc bus and therefore does not support normal device operation.

\newpage
