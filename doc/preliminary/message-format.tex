\subsection{Message Formats}\label{sec:messageFormat}
Data is sent between the parts of the Logic LID using 3 different types of messages: Device State Messages, PC Messages, and Transition Messages.
The purpose and format of each message is described below.

\subsubsection{Device Types}
There are multiple device types supported by the Logic LID.
These types are listed in Table \ref{tab:deviceTypes} along with their Type ID.
The Type ID is a 4-bit identifier for each device type.

\begin{table}[ht]
	\centering
	\begin{tabular}{|ll|}\hline
		\textbf{Type ID} & \textbf{Device Name} \\\hline
		0x0 & Undefined Device \textit{(Invalid Device)} \\
		0x1 & AND Gate \\
		0x2 & OR Gate \\
		0x3 & Inverter \\
		0x4 & Switch \\\hline
	\end{tabular}
	\caption{Device Types and Type IDs}
	\label{tab:deviceTypes}
\end{table}

\subsubsection{Device Orientations}
There are 4 possible device orientations: A, B, C, and D.
These orientations correspond to a specific Orientation ID as shown in Table \ref{tab:deviceOrientations}.
The Orientation ID is a 4-bit identifier for each orientation.

\begin{table}[ht]
	\centering
	\begin{tabular}{|ll|}\hline
		\textbf{Orientation ID} & \textbf{Orientation} \\\hline
		0x1 & A \\
		0x2 & B \\
		0x4 & C \\
		0x8 & D \\\hline
	\end{tabular}
	\caption{Device Orientations}
	\label{tab:deviceOrientations}
\end{table}

\subsubsection{Device State Messages}
Device State Messages are repeatedly read by the controller from the connected devices to determine the presence and state of the connected devices.
Device State Messages are sent via the \itwoc bus with a single core of the controller acting as the master and the devices acting as slaves.
The current bus mode, stored on the controller, is sent to the device as part of the message to determine which state data is read from the device.
Table \ref{tab:deviceStateMessage} describes the \itwoc bus events that make up a Device State Message.
Note that if NACK occurs a STOP condition is sent to release the \itwoc bus.
The device state data depends upon the current bus mode and is described in Table \ref{tab:deviceStateData}.

\begin{table}[ht]
	\centering
	\begin{tabular}{|lp{7cm}|}
		\hline
		\textbf{Event} & \textbf{Description} \\
		\hline
		Send START Condition & The master starts communication \\
		Send Device Address + W Bit & The address of the device to read the state from and WRITE bit \\
		Read ACK/NACK & If ACK the device at this address is connected, if NACK there is no device at this address (NACK ends the messages) \\
		Send Bus Mode Byte & Send the current bus mode \\
		Repeated START & Repeat start to prepare for reading state \\
		Send Device Address + R Bit & The address of the same device to read the state from \\
		Read ACK/NACK & If ACK continue, if NACK device is gone and end message \\
		Read Device State Data & This is the data, depending on the bus mode \\
		Send STOP & End the message \\
		\hline
	\end{tabular}
	\caption{Device State Message Format}
	\label{tab:deviceStateMessage}
\end{table}

\begin{table}[ht]
	\centering
	\begin{tabular}{|ll cccc cccc|}
		\hline
		\textbf{Mode} & \textbf{Description} & \multicolumn{8}{c|}{\textbf{Bits}} \\
		& & 7 & 6 & 5 & 4 & 3 & 2 & 1 & 0 \\\hline
		Build (0x01) & Type and Orientation &
			\multicolumn{4}{|c|}{Orientation} & \multicolumn{4}{c|}{Type} \\\hline
		Run (0x02) & IO State &
			\multicolumn{1}{|c|}{IN2}  &
			\multicolumn{1}{c|}{IN1} &
			\multicolumn{1}{|c|}{\textit{N/A}} &
			\multicolumn{1}{|c|}{\textit{N/A}} &
			\multicolumn{1}{|c|}{OUT} &
			\multicolumn{1}{c|}{D1} &
			\multicolumn{1}{|c|}{D2} &
			IN3 \\
		\hline
	\end{tabular}
	\caption{Device State Data}
	\label{tab:deviceStateData}
\end{table}

\subsubsection{PC Messages}
The controller sends messages to the software after reading in the state of a connected device using a PC Message.
This message is sent over an asynchronous serial interface to the PC software.
Note that these messages are send repeatedly and represent the current state of the connected devices when they are sent.
These messages do not capture the change in device state (such as a device being added) but rather they represent the current device state (such as a device being present).
It is up to the PC software to take this current state and determine the change in state.
A PC Message is composed of the following parts (in this order):

\begin{enumerate}
	\item \textbf{Message Start Indicator}: is a zero byte (0x00) followed by a non-zero byte (this non-zero byte is the first byte in the message).
	\item \textbf{Message Length}: 1 byte which contains the number of bytes in the message. This is the non-zero byte that is part of the start indicator. This is always non-zero since every message has at least 1 byte.
	\item \textbf{Message Type}: 1 byte indicating the message type, listed in Table \ref{tab:pcMessageTypes}.
	\item \textbf{Message Data}: 0 or more bytes containing the message data. For exact details about message data see the message types in Table \ref{tab:pcMessageTypes}.
	\item \textbf{Checksum}: 1 byte that is the sum of everything past the message length (the message type and message data) subtracted from 0xFF. The subtraction from 0xFF is to avoid all zero bytes evaluating to a valid checksum.
\end{enumerate}

PC messages are parsed without any prior knowledge of the controller state.
This means that the PC software may begin interpreting the message stream, from the serial port, at any point.
To avoid invalid data being received the checksum must match in order for a message to be acted upon.
If the checksum is invalid the byte stream is evaluated after the invalid message's start indicator (in the event that there was 0x00 in the message data and the PC software happened to start reading at that zero to non-zero transition).

Table \ref{tab:pcMessageTypes} lists the PC messages that may be sent by the controller and the data contained within.
The Type and Orientation data and the IO State data is the same as described in Table \ref{tab:deviceStateData} (and read directly from the devices).

\begin{table}[ht]
	\centering
	\begin{tabular}{|llp{10cm}|}\hline
		\textbf{Type} & \textbf{ID} & \textbf{Data} \\\hline
		Device Present & 0x01 & 2 Bytes: \begin{enumerate}
				\item The Device Address
				\item The Device Type and Orientation (see Table \ref{tab:deviceStateData})
			\end{enumerate} \\
		Device Missing & 0x02 & 1 Byte: \begin{enumerate}
				\item The (Missing) Device Address
			\end{enumerate} \\
		Device IO State & 0x03 & 2 Bytes \textit{(Only Run Mode)}: \begin{enumerate}
				\item The Device Address
				\item The Device IO State data (see Table \ref{tab:deviceStateData})
			\end{enumerate} \\
		\hline
	\end{tabular}
	\caption{PC Messages and Data}
	\label{tab:pcMessageTypes}
\end{table}

\subsubsection{Transition Messages}
The PC Messages, that are sent from the controller to the PC software, only represent the current state of the connected devices.
The PC software keeps track of these messages and detects changes in the device state.
The changing state is communicated to the rest of the software as a Transition Message.
Transition Messages exist only in the PC software and are not serialized over a communication stream.
Transition Messages can also be treated as events that occur due to changing device state.
Transition Messages are generated directly by comparing incoming PC messages to previously stored device state.
The following Transition Messages (events) are generated:

\begin{itemize}
	\item \textbf{Removed Device}: when a missing device becomes present due to a Device Present message.
	\item \textbf{Removed Device}: when a previously present device (from a Device Present message) is no longer present due to a Device Missing message.
	\item \textbf{IO Changed}: \textit{in run mode only} when the IO state of a device changes (any of the device inputs or outputs changes from the previously stored IO state).
\end{itemize}

\subsubsection{Mode Change Messages}
Currently, Mode Change Messages are the only message sent from the PC software to the controller.
These messages are made up of exactly one byte that is either 0x01 for build mode or 0x02 for run mode.
When the software starts build mode is the default.
Since the message is simply one byte there is no special encapsulation for Mode Change messages.
If a Mode Change message is received by the controller that is neither 0x01 or 0x02 then the message is ignored.
