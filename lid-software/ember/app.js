define('app', ['l2:app'], function(L2Application) {
	return L2Application.create({
		rootElement: 'body'
	});
});