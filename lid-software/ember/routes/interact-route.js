var lid = require('./src/index');

define('route:interact', ['slc:loader'], function(Loader) {
	return Ember.Route.extend({

		// serialize returns the URL params.
		serialize: function(model) {
			return {
				port_name: model.comName
			};
		},

		// setupController sets up the bus from the port.
		setupController: function(controller, port) {
			controller.set('debugMessages', []);
			controller.set('bus', new lid.Bus(port));
			var openPromise = controller.get('bus').open();

			controller.set('openLoader', Loader.load(openPromise));
			//controller.set('connectedDevices', []);
		}

	});
});