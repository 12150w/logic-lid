define('route:index', function() {
	return Ember.Route.extend({

		// setupController sets the controller to reload ports.
		setupController: function(controller, model) {
			controller.send('reloadPorts');
			controller.set('model', model);
		}

	});
});