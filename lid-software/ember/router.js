define('router', ['app'], function(app) {
	app.Router.map(function() {
		this.route('interact', {path: '/interact/:port_name'});
	});
});