var moment = require('moment');

define('controller:interact', function() {
	return Ember.Controller.extend({

		// bus is the lid.Bus instance to interact with (set in setupController).
		bus: null,

		// openLoader is the loader for opening the bus (set in setupController).
		openLoader: null,

		// debugMessages is the list of debug messages (reset in setupController).
		debugMessages: null,

		// debugLimit is the maximum number of debug messages allowed.
		debugLimit: 100,

		// mode is the current mode of the bus (text).
		mode: 'Build',

		// connectedDevices is a list of connected devices.
		connectedDevices: [],

		// isRunMode is true when in run mode.
		isRunMode: Ember.computed('mode', function() {
			return this.get('mode') === 'Run';
		}),

		// nextMode is the next mode that can be switched to.
		nextMode: Ember.computed('mode', function() {
			var mode = this.get('mode');

			if(mode === 'Build') {
				return 'Run';
			} else {
				return 'Build';
			}
		}),

		// attachMessageListener attaches listeners for changed busses.
		attachMessageListener: Ember.observer('bus', function() {
			var bus = this.get('bus');
			if(bus == null) return;
			var devices = this.get('connectedDevices');

			var self = this;
			bus.on('addedDevice', function(device) {
				if(device.typeId === 0) return;
				self.addDebug('New Device', device);

				if(devices.indexOf(device) < 0) {
					devices.pushObject(device);
				}
			});
			bus.on('removedDevice', function(device) {
				if(device.typeId === 0) return;
				self.addDebug('Removed Device', device);

				if(devices.indexOf(device) > 0) {
					devices.removeObject(device);
				}
			});
			bus.on('ioChanged', function(device) {
				self.addDebug('IO Change', device);
			});
		}),

		// addDebug adds a debug message for a certain device.
		addDebug: function(eventName, data) {
			if(this.get('debugMessages') == null) return;
			var message = {
				eventName: eventName,
				data: data,
				time: moment().format('h:m:s.S A'),
				color: null,
				infoTemplate: 'event/' + Ember.String.camelize(eventName)
			};

			switch(eventName) {
				case 'New Device':
					message.color = 'green';
					break;
				case 'Mode Switch':
					message.color = 'blue';
					break;
				case 'Removed Device':
					message.color = 'red';
					break;
			}

			this.get('debugMessages').unshiftObject(message);

			// cut old messages if over debug limit
			var msgLimit = this.get('debugLimit');
			if(this.get('debugMessages').length > msgLimit) {
				this.set('debugMessages', this.get('debugMessages').slice(0, msgLimit));
			}
		},

		actions: {

			// toggleModes changes the mode from build to run or from run to build.
			toggleModes: function() {
				var nextMode = this.get('nextMode');

				var modePromise;
				if(nextMode === 'Run') {
					modePromise = this.get('bus').runMode();
				} else {
					modePromise = this.get('bus').buildMode();
				}

				var self = this;
				modePromise.then(function() {
					self.addDebug('Mode Switch', nextMode);
					self.set('mode', nextMode);
				});
			}

		}

	});
});