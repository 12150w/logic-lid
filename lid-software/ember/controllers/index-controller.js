var lid = require('./src/index');

define('controller:index', ['slc:loader'], function(Loader) {
	return Ember.Controller.extend({

		// portLoader is the loader for listing ports.
		portLoader: null,

		actions: {

			// reloadPorts reloads the available ports, setting the port loader.
			reloadPorts: function() {
				var portPromise = lid.serial.list();

				this.set('portLoader', Loader.load(portPromise));
			},

			// simulate creates a simulated Logic LID.
			simulate: function() {
				this.transitionToRoute('interact', new lid.serial.Port(null, {simulate: true}));
			},

			// interact creates a Logic LID given a port name.
			interact: function(portName) {
				this.transitionToRoute('interact', new lid.serial.Port(portName));
			},

			// portChanged runs when the user changes the selected port.
			portChanged: function() {
				console.log(arguments);
			}

		}

	});
});