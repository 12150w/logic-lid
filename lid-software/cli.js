/*
	LID Software CLI

	This interface allows examining the messages on the LID bus from a command
	line interface. It starts the bus in build mode, waits 3 seconds to detect
	any connected devices, then transitions to run mode.
*/
var lid = require('./src/index'),
	args = require('minimist')(process.argv.slice(2));

// Connect to the first available port
lid.serial.list().then(function(portNames) {
	var mainBus = new lid.Bus(new lid.serial.Port(portNames[0], {
		simulate: args.sim === true
	}));

	return mainBus.open().then(function() {
		if(args.sim === true) {
			console.log('Opened Simulated Bus');
		} else {
			console.log('Opened ' + portNames[0]);
		}

		setTimeout(function() {
			mainBus.runMode().then(function() {
				console.log('Entered Run Mode');
			});
		}, 3000);
	});
}).done();