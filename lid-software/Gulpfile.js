var gulp = require('gulp'),
	compile = require('gulp-level2-ember'),
	rename = require('gulp-rename');

// Compiles the Level2 Ember Application
gulp.task('compile', function() {
	return gulp.src('ember/**/*.*')
		.pipe(compile({
			templateCompiler: 'ember-source/dist/ember-template-compiler'
		}))
		.pipe(rename('app.js'))
		.pipe(gulp.dest('dist/static/js'));
});
