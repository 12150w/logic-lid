/*
	LID Software Main Module

	This is the main, exported module of the LID Software.
*/
module.exports = {
	serial: require('./serial'),
	Bus: require('./bus'),
	Device: require('./device')
};