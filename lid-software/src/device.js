/*
	Device Module

	The device module provides the Device object which manages an individual device.
*/
var base = require('level2-base');

// Device is an object that manages a device, this is exported from the module.
var Device = base.Class.extend({

	// address is the device's address number.
	address: 0,

	// typeId is the device type identifier.
	typeId: 0,

	// typeName is the type name of the device.
	typeName: null,

	// ioState is the IO state octet (byte) of this device (null means the IO state is unknown).
	ioState: null,

	// currentState is the currently parsed ioState.
	currentState: null,

	// orientation is the orientation name (either 'A', 'B', 'C', or 'D') of the device.
	//   If orientation is null that means this is an invalid orientation.
	orientation: null,

	// init creates a device given its address and type data.
	init: function(address, typeData) {
		this.address = address;
		this.typeId = typeData & 0x0F;

		var orientationId = (typeData & 0xF0) >> 1;
		if(Device.DEVICE_ORIENTATIONS[orientationId] != null) {
			this.orientation = Device.DEVICE_ORIENTATIONS[orientationId];
		}

		if(Device.DEVICE_TYPES[this.typeId] == null) this.typeName = Device.DEVICE_TYPES[0];
		else this.typeName = Device.DEVICE_TYPES[this.typeId];
	},

	// typeName returns the device type name.
	typeName: function() {
		if(Device.DEVICE_TYPES[this.typeId] == null) {
			return Device.DEVICE_TYPES[0];
		} else {
			return Device.DEVICE_TYPES[this.typeId];
		}
	},

	// state returns the parsed IO state as an object.
	state: function() {
		var state = {};
		if(this.ioState == null) return state;

		state.in1 = (this.ioState & 0x40) > 0;
		state.in2 = (this.ioState & 0x80) > 0;
		state.in3 = (this.ioState & 0x01) > 0;
		state.out = (this.ioState & 0x08) > 0;
		state.d1  = (this.ioState & 0x04) > 0;
		state.d2  = (this.ioState & 0x02) > 0;

		return state;
	}

});

// DEVICE_TYPES is a map between device type ids and the device type names.
Device.DEVICE_TYPES = {
	0: 'Unknown Device',
	1: 'AND Gate',
	2: 'OR Gate',
	3: 'Inverter',
	4: 'Switch'
};

// DEVICE_ORIENTATIONS is a map between device orientation ids and the device orientation names.
Device.DEVICE_ORIENTATIONS = {0x08: 'A', 0x10: 'B', 0x20: 'C', 0x40: 'D'};

module.exports = Device;
