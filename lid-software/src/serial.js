/*
	Serial Interface

	This module provides access to serial ports.
	It essentially wraps the serial port access library (serialport)
	and parses/serialized data that moves on the serial port.
*/
var SerialPort = require('serialport'),
	base = require('level2-base'),
	q = require('q');

// list returns a promise that resolves with a list of available serial ports (strings).
module.exports.list = function() {
	return q.nfcall(SerialPort.list).then(function(portObjects) {
		return portObjects.filter(function(port) {
			return port.manufacturer != null;
		}).map(function(port) {
			return port.comName;
		});
	});
};

// Port is a serial port identified by its com name.
module.exports.Port = base.Class.extend({

	// device is the underlying serialport library object.
	device: null,

	// comName is the opened port name,
	comName: null,

	// maxLength is the maximum number of bytes allowed in the buffer.
	maxLength: 50,

	// init instantiates the device.
	init: function(comName, options) {
		this.comName = comName;
		options = options || {};
		this._simulate = options.simulate;

		if(this._simulate !== true) {
			this.device = new SerialPort(comName, {
				baudRate: 115200,
				autoOpen: false
			});
		}
		this._buf = Buffer.alloc(0);
		this._listeners = {
			'message': []
		};
	},

	// open connects to the device on the serial port and returns a promise that resolves once connected.
	open: function() {
		var openDefer = q.defer(),
			self = this;
		
		// simulate connection if sim mode
		if(this._simulate === true) {
			var self = this;

			var intervalFunction;
			if(typeof('window') !== undefined) intervalFunction = window.setInterval;
			else intervalFunction = setInterval;

			intervalFunction(function() {
				self._simulateMessage();
			}, 1000);
			openDefer.resolve();
			return openDefer.promise;
		}

		this.device.on('data', function(data) {
			self._scan(data);
		});
		this.device.open(function() {
			openDefer.resolve();
			openDefer = null;
		});
		this.device.on('error', function(err) {
			if(openDefer == null) return;
			openDefer.reject(err);
		});

		return openDefer.promise;
	},

	// on attaches an event listener for a certain event.
	on: function(event, listener) {
		this._listeners[event].push(listener);
	},

	// sendByte sends a single byte and returns a promise that resolves when the send buffer is drained.
	sendByte: function(byte) {
		var device = this.device;
		if(this._simulate === true) {
			this._simMode = byte;
			return q();
		}

		return q.ninvoke(device, 'write', Buffer.alloc(1, byte)).then(function() {
			return q.ninvoke(device, 'drain');
		});
	},

	// _buf is the internal buffer containing the data being scanned.
	_buf: null,

	// _listeners is a map by event name to event listeners.
	_listeners: null,

	// _simulate is true if this is a simulated port.
	_simulate: false,

	// _simMode is the mode of the bus to simulate.
	_simMode: 1,

	// _simIOState is the last simulated IO state.
	_simIOState: 0x00,

	// _scan reads in the next buffer of available data and emits messages for any complete messages.
	_scan: function(data) {
		if(this._buf.length + data.length > this.maxLength) {
			this._buf = this._buf.slice(this._buf.length + data.length - this.maxLength);
		}
		this._buf = Buffer.concat([this._buf, data], this._buf.length + data.length);
		
		for(var i=0; i<this._buf.length; i++) {
			
			// check for 0x00 then non-zero (i at 0x00)
			if(this._buf[i] != 0x00) continue;
			if(i+1 >= this._buf.length) {
				i++;
				continue;
			}
			if(this._buf[i+1] == 0x00) continue;
			i++;

			// read the data count (i at data count)
			var dataCount = this._buf[i];
			if(i+dataCount+1 >= this._buf.length) {
				i += dataCount + 1;
				continue;
			}
			i++;

			// read the data since it is all in the buffer (i at first data byte)
			var dataSum = 0;
			var message = new module.exports.Message(this._buf[i], this._buf.slice(i+1, i+dataCount));
			for(var j=0; j<dataCount; j++) {
				dataSum += this._buf[i+j];
			}

			// verify the checksum (i at first data byte)
			if(this._buf[i+dataCount] === 0xFF-dataSum) {
				this._emit('message', [message]);
			}
			i += dataCount + 1;

		}
	},

	// _emit sends a message and arguments to all listeners of an event.
	_emit: function(event, eventArguments) {
		var self = this;

		this._listeners[event].forEach(function(listener) {
			listener.apply(this, eventArguments);
		});
	},

	// _simulateMessage generates a sample message, appropriate to the mode, and sends it.
	_simulateMessage: function() {
		var message;

		// Send device present message
		if(this._simMode == 1) {
			message = new module.exports.Message(
				module.exports.Message.TYPES.DEVICE_PRESENT,
				Buffer.from([0x02, 0x13])
			);
		}

		// Send IO change messasge
		else if(this._simMode == 2) {
			var nextIOState;
			if(this._simIOState == 0x00) {
				nextIOState = 0x08;
			} else {
				nextIOState = 0x00;
			}

			message = new module.exports.Message(
				module.exports.Message.TYPES.DEVICE_IOSTATE,
				Buffer.from([0x02, nextIOState])
			);

			this._simIOState = nextIOState;
		}

		if(message != null) this._emit('message', [message]);
	}

});

// Message is a single message on received or sent on the serial port.
module.exports.Message = base.Class.extend({

	// type is the type identifier as a number.
	type: 0,

	// data is the buffer containing the message data.
	data: null,

	// init instantiates a message given its type and data.
	init: function(type, data) {
		this.type = type;
		this.data = data;
	},

	// hexData returns the hexidecimal value, in uppercase, at a given index.
	hexData: function(index) {
		return this.data.slice(index, index+1).toString('hex').toUpperCase();
	}

});

// Message.TYPES is a map of message type identifiers to their ids.
module.exports.Message.TYPES = {
	DEVICE_PRESENT: 1,
	DEVICE_MISSING: 2,
	DEVICE_IOSTATE: 3
};
