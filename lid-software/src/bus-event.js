/*
	Bus Event

	A bus event is the object that is emitted by the Bus when something happens on the LID Bus.
*/
var base = require('level2-base');

module.exports = base.Class.extend({

});