/*
	Bus Module

	This module provides the Bus object which can be used to manage
	a bus of LID devices.
*/
var base = require('level2-base'),
	serial = require('./serial'),
	Device = require('./device');

// Bus is an object which can be used to manage a bus of LID devices.
module.exports = base.Class.extend({

	// port is the serial.Port the controller for this bus is connected to.
	port: null,

	// init instantiates a bus given a port the controller is connected to.
	init: function(port) {
		this.port = port;
		this._devices = {};

		var self = this;
		this.port.on('message', function() {
			self._onMessage.apply(self, arguments);
		});

		this._listeners = {
			addedDevice: [],
			removedDevice: [],
			ioChanged: []
		};
	},

	// open begins communication with the controller.
	//   returnins a promise that resolves when the connection is complete.
	open: function() {
		var self = this;

		return this.port.open().then(function() {
			return self.buildMode();
		});
	},

	// buildMode switches the bus to build mode and returns a promise that resolves when the message is sent.
	buildMode: function() {
		this._currentMode = 1;
		return this.port.sendByte(1);
	},

	// runMode switches the bus to run mode and returns a promise that resolves when the message is sent.
	runMode: function() {
		this._currentMode = 2;
		return this.port.sendByte(2);
	},

	// on attaches a listener for an event.
	on: function(eventName, listener) {
		this._listeners[eventName].push(listener);
	},

	// _devices is a map between the device address and the Device instance for all connected devices.
	_devices: null,

	// _currentMode is the currently active mode ID of the bus.
	_currentMode: 1,

	// _listeners is a map between event types and their listeners.
	_listeners: null,

	// _onMessage is the message handler for this bus (it is called whenever a message is received).
	_onMessage: function(message) {

		// Device Added
		if(message.type == serial.Message.TYPES.DEVICE_PRESENT && this._devices[message.data[0]] == null && this._currentMode == 1) {
			var newDevice = new Device(message.data[0], message.data[1]);
			this._devices[message.data[0]] = newDevice;

			this._emit('addedDevice', [newDevice]);
		}

		// Device Removed
		else if(message.type == serial.Message.TYPES.DEVICE_MISSING && this._devices[message.data[0]] != null && this._currentMode == 1) {
			var lostDevice = this._devices[message.data[0]];
			delete this._devices[message.data[0]];

			this._emit('removedDevice', [lostDevice]);
		}

		// IO Change
		else if(message.type == serial.Message.TYPES.DEVICE_IOSTATE && this._devices[message.data[0]] != null && this._currentMode == 2) {
			var ioDevice = this._devices[message.data[0]];
			if((ioDevice.ioState&0x08) != (message.data[1]&0x08)) {
				ioDevice.ioState = message.data[1];
				ioDevice.currentState = ioDevice.state();
				
				this._emit('ioChanged', [ioDevice]);
			}
		}

	},

	// _emit emits an event to listeners.
	_emit: function(eventName, args) {
		var listeners = this._listeners[eventName];
		if(listeners == null) return;

		var self = this;
		listeners.forEach(function(listener) {
			listener.apply(self, args);
		});
	}

});
