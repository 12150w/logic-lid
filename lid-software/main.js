var electron = require('electron'),
	args = require('minimist')(process.argv.slice(2));

// win is the main browser window.
var win;

// createMainWindow creates the main window.
var createMainWindow = function() {
	if(win != null) return;

	win = new electron.BrowserWindow({
		kiosk: true,
		title: 'Logic LID'
	});
	if(args.debug !== true) win.setMenu(null);
	if(args.debug === true) win.webContents.openDevTools();
	win.loadURL('file://' + __dirname + '/index.html');

	electron.globalShortcut.register('Escape', function() {
		electron.app.quit();
	});
};

// Listeners that create the main window.
electron.app.on('ready', createMainWindow);
