# Propeller GCC Makefile
#   This makefile is to build C applications for the P8X32A microcontroller.

# Clock speed
CLOCK_SPEED=5mhz

# Clock mode
CLOCK_MODE=xtal1+pll16x


# The name of the output
OUTPUT=firmware

# The GCC compiler to use
CC=propeller-elf-gcc

# Paths containing simple tools
SLIB_PATH=simple-lib/Learn/Simple Libraries
CLIBS=\
	-I "$(SLIB_PATH)/Utility/libsimpletools" \
	-I "$(SLIB_PATH)/TextDevices/libsimpletext" \
	-I "$(SLIB_PATH)/Protocol/libsimplei2c"
SLIB_SRC= "$(SLIB_PATH)/Utility/libsimpletools/libsimpletools.c"
#SOURCES+= "$(SLIB_PATH)/Utility/libsimpletools/libsimpletools.c" "$(SLIB_PATH)/Utility/libsimpletools/source/addfiledriver.c" "$(SLIB_PATH)/Utility/libsimpletools/source/cogrun.c" "$(SLIB_PATH)/Utility/libsimpletools/source/cogend.c" "$(SLIB_PATH)/Utility/libsimpletools/source/cognum.c" "$(SLIB_PATH)/Utility/libsimpletools/source/count.c" "$(SLIB_PATH)/Utility/libsimpletools/source/dac.c" "$(SLIB_PATH)/Utility/libsimpletools/source/eeprom_init.c" "$(SLIB_PATH)/Utility/libsimpletools/source/eeprom_initSclDrive.c" "$(SLIB_PATH)/Utility/libsimpletools/source/eeprom_getByte.c" "$(SLIB_PATH)/Utility/libsimpletools/source/eeprom_getFloat.c" "$(SLIB_PATH)/Utility/libsimpletools/source/eeprom_getInt.c" "$(SLIB_PATH)/Utility/libsimpletools/source/eeprom_getStr.c" "$(SLIB_PATH)/Utility/libsimpletools/source/eeprom_putByte.c" "$(SLIB_PATH)/Utility/libsimpletools/source/eeprom_putFloat.c" "$(SLIB_PATH)/Utility/libsimpletools/source/eeprom_putInt.c" "$(SLIB_PATH)/Utility/libsimpletools/source/eeprom_putStr.c" "$(SLIB_PATH)/Utility/libsimpletools/source/endianSwap.c" "$(SLIB_PATH)/Utility/libsimpletools/source/freqout.c" "$(SLIB_PATH)/Utility/libsimpletools/source/getDirection.c" "$(SLIB_PATH)/Utility/libsimpletools/source/getOutput.c" "$(SLIB_PATH)/Utility/libsimpletools/source/getOutputs.c" "$(SLIB_PATH)/Utility/libsimpletools/source/getState.c" "$(SLIB_PATH)/Utility/libsimpletools/source/getStates.c" "$(SLIB_PATH)/Utility/libsimpletools/source/getDirections.c" "$(SLIB_PATH)/Utility/libsimpletools/source/high.c" "$(SLIB_PATH)/Utility/libsimpletools/source/i2c_busy.c" "$(SLIB_PATH)/Utility/libsimpletools/source/i2c_init.c" "$(SLIB_PATH)/Utility/libsimpletools/source/i2c_in.c" "$(SLIB_PATH)/Utility/libsimpletools/source/i2c_out.c" "$(SLIB_PATH)/Utility/libsimpletools/source/input.c" "$(SLIB_PATH)/Utility/libsimpletools/source/low.c" "$(SLIB_PATH)/Utility/libsimpletools/source/mark.c" "$(SLIB_PATH)/Utility/libsimpletools/source/pause.c" "$(SLIB_PATH)/Utility/libsimpletools/source/pulseIn.c" "$(SLIB_PATH)/Utility/libsimpletools/source/pulseOut.c" "$(SLIB_PATH)/Utility/libsimpletools/source/pwm.c" "$(SLIB_PATH)/Utility/libsimpletools/source/rcTime.c" "$(SLIB_PATH)/Utility/libsimpletools/source/reverse.c" "$(SLIB_PATH)/Utility/libsimpletools/source/sddriverconfig.c" "$(SLIB_PATH)/Utility/libsimpletools/source/setDirection.c" "$(SLIB_PATH)/Utility/libsimpletools/source/setDirections.c" "$(SLIB_PATH)/Utility/libsimpletools/source/setIoDt.c" "$(SLIB_PATH)/Utility/libsimpletools/source/setOutput.c" "$(SLIB_PATH)/Utility/libsimpletools/source/setOutputs.c" "$(SLIB_PATH)/Utility/libsimpletools/source/setPauseDt.c" "$(SLIB_PATH)/Utility/libsimpletools/source/setTimeout.c" "$(SLIB_PATH)/Utility/libsimpletools/source/shiftIn.c" "$(SLIB_PATH)/Utility/libsimpletools/source/shiftOut.c" "$(SLIB_PATH)/Utility/libsimpletools/source/squareWave.c" "$(SLIB_PATH)/Utility/libsimpletools/source/term_cmd.c" "$(SLIB_PATH)/Utility/libsimpletools/source/timeout.c" "$(SLIB_PATH)/Utility/libsimpletools/source/timeTicks.c" "$(SLIB_PATH)/Utility/libsimpletools/source/toggle.c" "$(SLIB_PATH)/Utility/libsimpletools/source/wait.c"

# The GCC flags to always use
CFLAGS=-Os -Wall -mlmm -m32bit-doubles $(CLIBS)

# The propeller loader
LOADER=propeller-load

# The flags for the loader
LOAD_FLAGS=-D clkfreq=$(CLOCK_SPEED) -D clkmode=$(CLOCK_MODE)


#----------------------------------------#
# Build Targets                          #
#----------------------------------------#

# The object files to build
OBJECTS=$(SOURCES:.c=.o)

# Final linking
$(OUTPUT).elf: $(OBJECTS)
	$(CC) $(CFLAGS) -o $@ $(OBJECTS)

# Individual object compile
$(OBJECTS): %o: $(OBJECTS:.o=.c) $(OBJECTS:.o=.h) Makefile
	$(CC) $(CFLAGS) -c -o $@ $(patsubst %.o,%.c,$@)

# Upload to board to run
upload: $(OUTPUT).elf
	$(LOADER) $(LOAD_FLAGS) -r $(OUTPUT).elf

# Upload to EEPROM and then run
flash: $(OUTPUT).elf
	$(LOADER) $(LOAD_FLAGS) -e -r $(OUTPUT).elf
terminal: $(OUTPUT).elf
	$(LOADER) $(LOAD_FLAGS) -t -r $(OUTPUT).elf

# Remove all outputs
clean:
	rm -f $(OUTPUT).hex
	rm -f $(OUTPUT).elf
	rm -f $(OBJECTS)

.PHONY: clean upload flash terminal
