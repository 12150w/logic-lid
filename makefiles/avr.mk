# AVR-GCC Makefile
#   This makefile contains targets for avr compilation.
#   To use the targets here, define at least MMCU, PART, F_CPU, and SOURCES.
#   To use programming (the upload target) define PROGRAMMER, AVRDUDE_PART, and AVRDUDE_FLAGS for your programmer.
#   Note that these variables must be defined BEFORE including this file.

# (Optional) Include User Overrides
-include ../../makefiles/user.mk

#----------------------------------------#
# User Definitined Variables             #
#----------------------------------------#

# LID_TYPE is the value to define as "LID_TYPE_<LID_TYPE>"
LID_TYPE?=AND_GATE

# MMCU is the avr-gcc microcontroller name.
MMCU?=atmega32

# F_CPU is the frequency that the device is operating at (as an integer).
F_CPU?=8000000

# SOURCES is the list of C files (ending in ".c") to compile
SOURCES?=

# AVRDUDE_PART is the part name in avrdude.
AVRDUDE_PART?=m32

# PROGRAMMER is the avrdude programmer name to use
PROGRAMMER?=usbtiny

# AVRDUDE_FLAGS are the flags to pass to avrdude (use to set up your programmer)
AVRDUDE_FLAGS?=

# COMPILED_OUTPUT is the output file base (no extension) that the linked output will be named with.
COMPILED_OUTPUT?=firmware


#----------------------------------------#
# Internal Variables (Don't Override)    #
#----------------------------------------#

# OBJECTS is the list of object files to compile.
OBJECTS=$(SOURCES:.c=.o)

# AVR_GCC is the avr-gcc executable to use.
AVR_GCC=avr-gcc

# AVR_CFLAGS are the compile flags to pass to avr-gcc.
AVR_CFLAGS=-Wall -O1 -g -std=c99 -mmcu=$(MMCU) -DF_CPU=$(F_CPU) -DLID_TYPE_$(LID_TYPE) -DLID_ADDRESS=$(LID_ADDRESS)

# AVR_OBJCOPY is the avr-objcopy executable to use.
AVR_OBJCOPY=avr-objcopy

# AVRDUDE is the avrdude executable to use.
AVRDUDE=avrdude

# AVRDUDE_DEF_FLAGS are the flags to always include for avrdude
AVRDUDE_DEF_FLAGS?=-e -D -v -B 10 -p $(AVRDUDE_PART) -c $(PROGRAMMER)


#----------------------------------------#
# Build Targets                          #
#----------------------------------------#
.PHONY: clean upload build

# Linking Targets
$(COMPILED_OUTPUT).hex: $(COMPILED_OUTPUT).elf
	$(AVR_OBJCOPY) -O ihex -j .text -j .data $< $@
$(COMPILED_OUTPUT).elf: $(OBJECTS)
	$(AVR_GCC) $(AVR_CFLAGS) -o $@ $(OBJECTS)
build: $(COMPILED_OUTPUT).hex

# Compile Target
$(OBJECTS): %o: $(OBJECTS:.o=.c) $(OBJECTS:.o=.h) Makefile
	$(AVR_GCC) $(AVR_CFLAGS) -c -o $@ $(patsubst %.o,%.c,$@)

# Flashing Target
upload: $(COMPILED_OUTPUT).hex
	$(AVRDUDE) $(AVRDUDE_DEF_FLAGS) $(AVRDUDE_FLAGS) -U flash:w:$(COMPILED_OUTPUT).hex:i

# Clean Target
clean:
	rm -f $(COMPILED_OUTPUT).hex
	rm -f $(COMPILED_OUTPUT).elf
	rm -f $(OBJECTS)
