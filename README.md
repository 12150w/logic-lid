# Logic LID
This project contains the hardware design and software for the Logic LID.


## Software
The contents of this project are created using a variety of software.
In order to contribute to the project install the required software depending on where you want to contribute.

### Setup Git Software
**Git must be set up** regardless of which part of the project you are contributing to.

1. Download and install Git ([download here](https://git-scm.com/))
    * On Windows you should see a program named "Git Bash" after installation
2. Generate your SSH key pair using Git Bash and following [this tutorial](https://git-scm.com/book/en/v1/Git-on-the-Server-Generating-Your-SSH-Public-Key)
    * Probably run something like `ssh-keygen -t rsa` and accept the defaults
3. Copy the public key (probably named `id_rsa.pub`) to your [GitLab Profile SSH Keys](https://gitlab.com/profile/keys)
    * You will have to create a GitLab account to do this, it is fairly painless just provide a username and email
4. Verify everything is set up properly by running the following command in Git Bash: `ssh git@gitlab.com`
    * If you see `Welcome to GitLab, <USERNAME>!` everything is set up
    * If you see an error message verify that your ssh key is generated (`id_rsa` and `id_rsa.pub` exist in the `~/.ssh` folder) and that you copied the contents of `id_rsa.pub` to your GitLab profile.
5. Download the project from GitLab by following the steps in the Contributing Section below.

### Device Development Software
The device files are stored in the `generic-device` folder.
They require the following software:

1. To edit schematics and board layouts download and install KiCad from [kicad-pcb.org](http://kicad-pcb.org/download/)
2. To develop device firmware setup the AVR-GCC toolchain
    * For Windows install [WinAVR](https://sourceforge.net/projects/winavr/)

### Base Board Development Software
The base board sources are stored in the `controller` (main controller) and `terminal` (device socket) folders.

1. To edit schematics and board layouts download and install KiCad from [kicad-pcb.org](http://kicad-pcb.org/download/)
2. To edit firmware install the Propeller IDE ([download here](http://developer.parallax.com/propelleride/))

### Documentation Software
Documentation is stored in the `doc` as a LaTeX document.
To edit and compile the documentation install the following:

1. Install a LaTeX distribution such as MiKTeX ([download MiKTeX](http://miktex.org/)).
2. (Optional) Install a LaTeX file editor to make your life easier, [TeXstudio](http://www.texstudio.org/) is a good option.
    * You can also use any text editor and run `pdflatex` manually but TeXstudio is easier


## Contributing
Once you have set up the software necessary for the piece you are working on commit your changes using the normal Git workflow.
Currently all development takes place on the `master` (default) branch.

### Cloning the Project
Once you have Git set up you must clone (download) the project to edit.
**You only have to do this once** the first time you set up the project.

1. Open Git Bash (on Windows)
2. Navigate to the location you want to store the project using the `cd` command
    * For example if you want to store the project in the Documents folder navigate there using `cd ~/Documents`
3. Clone (download) the project by running `git clone git@gitlab.com:12150w/logic-lid.git`
4. Verify the project was cloned (downloaded) by verifying that a folder named "logic-lid" was created where you ran `git clone` from

### Committing Changes
After cloning the project the process of commiting changes is short.
The following steps assume you have opened Git Bash and navigated inside the "logic-lid" project folder.
Once you have made changes to files in the repository follow these steps to push your changes to GitLab.

1. Run `git pull` to get the latest version of the master branch
2. See what files have changed by running `git status`
3. Stage the files you want to commit by using `git add`
    * To stage all changed files run `git add --all`
4. Create a local commit by running `git commit` and **don't forget to include a commit message** with the `-m` flag
    * For example run: `git commit -m "COMMIT MESSAGE HERE"`
5. Sync your local repository with GitLab by running `git push`
    * If someone else has already pushed to `master` before you pushed you will get an error message and should run `git pull` then try running `git push` again.
