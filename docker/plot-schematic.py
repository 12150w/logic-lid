'''
	Plot Schematic
	
	Plots a schematic from eeschema using Xvfb to simulate the display.
	Pass in the schematic file as the first argument to the script.
'''
import subprocess
import sys
import time
from xvfbwrapper import Xvfb

# PopenContext extends Popen to close file descriptors.
class PopenContext(subprocess.Popen):
	def __enter__(self):
		return self
	def __exit__(self, type, value, traceback):
		if self.stdout:
			self.stdout.close()
		if self.stderr:
			self.stderr.close()
		if self.stdin:
			self.stdin.close()
		if type:
			self.terminate()
		self.wait()

# xdotool runs the xdotool with a command
def xdotool(command):
	return subprocess.check_output(['xdotool'] + command)

# run_till_ok runs xdotool till succeeds
def run_till_ok(command, retries=20, delay=0.5):
	for i in range(retries):
		try:
			xdotool(command)
			return
		except subprocess.CalledProcessError:
			pass
		time.sleep(delay)

with Xvfb(width=800, height=600, colordepth=24):
	with PopenContext(['recordmydesktop', '--no-sound', '--no-frame', '--on-the-fly-encoding', '-o', 'recording.ogv']) as record_proc:
		with PopenContext(['eeschema', sys.argv[1]]) as eeschema_proc:
			try:
				run_till_ok(['search', '--onlyvisible', '--class', 'eeschema'])
				xdotool(['search', '--onlyvisible', '--class', 'eeschema', 'windowfocus'])
				xdotool(['key', 'alt+f', 'p', 'p'])
				run_till_ok(['search', '--name', 'Plot'])
				xdotool(['search', '--name', 'Plot', 'windowfocus'])
				xdotool(['type', 'plot'])
				xdotool(['key', 'Tab', 'Tab', 'Tab', 'Tab', 'Tab', 'Up', 'Up', 'Up', 'space', 'Return'])
				time.sleep(1.2)
				
				eeschema_proc.terminate()
			
			except subprocess.CalledProcessError as err:
				time.sleep(1)
				eeschema_proc.terminate()
				record_proc.terminate()
				
				raise err
				
		record_proc.terminate()
