EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:LID
LIBS:gate-through-hole-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 3 3
Title "Logic LID Device"
Date "2017-05-03"
Rev "1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ATTINY261A U2
U 1 1 588D2B1A
P 3550 3300
F 0 "U2" H 2550 2250 60  0000 C CNN
F 1 "ATTINY261A" H 3550 4350 60  0000 C CNN
F 2 "Housings_DIP:DIP-20_W7.62mm" H -2800 -1900 60  0001 C CNN
F 3 "http://www.atmel.com/Images/Atmel-2588-8-bit-AVR-Microcontrollers-tinyAVR-ATtiny261-ATtiny461-ATtiny861_Datasheet.pdf" H -2800 -1900 60  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Microchip/ATTINY261A-PU/?qs=%2fha2pyFadujN3KyhwdR9nLvVCRCN90eo0FYEcIwhuugKFJe7KtwIeA%3d%3d" H 3550 3300 60  0001 C CNN "Order Link"
	1    3550 3300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 588D3494
P 1950 4600
F 0 "#PWR012" H -500 400 50  0001 C CNN
F 1 "GND" H 1955 4427 50  0000 C CNN
F 2 "" H -500 650 50  0001 C CNN
F 3 "" H -500 650 50  0001 C CNN
	1    1950 4600
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR013
U 1 1 588D3691
P 1950 2050
F 0 "#PWR013" H -1350 -1550 50  0001 C CNN
F 1 "+3.3V" H 1965 2223 50  0000 C CNN
F 2 "" H -1350 -1400 50  0001 C CNN
F 3 "" H -1350 -1400 50  0001 C CNN
	1    1950 2050
	1    0    0    -1  
$EndComp
Text HLabel 8550 5700 2    60   BiDi ~ 0
SDA
Text HLabel 8550 6100 2    60   BiDi ~ 0
SCL
Text HLabel 4950 2500 2    60   Input ~ 0
IN3
Text HLabel 4950 3100 2    60   Input ~ 0
IN2
Text HLabel 4950 4100 2    60   Input ~ 0
IN1
Text HLabel 8550 2700 2    60   Input ~ 0
RA
Text HLabel 8550 2800 2    60   Input ~ 0
RB
Text HLabel 8550 2900 2    60   Input ~ 0
RC
Text HLabel 8550 3000 2    60   Input ~ 0
RD
$Comp
L R R10
U 1 1 588D5576
P 8200 2700
F 0 "R10" V 8250 2900 50  0000 C CNN
F 1 "20k" V 8200 2700 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 230 450 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/351/Xicon_09142016_291_293_299_CF-RC_series-1002354.pdf" H 300 450 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Xicon/299-20K-RC/?qs=sGAEpiMZZMu61qfTUdNhG3dwkzs8iXRmPBZRXkWwSxg%3d" V 8200 2700 60  0001 C CNN "Order Link"
	1    8200 2700
	0    1    1    0   
$EndComp
$Comp
L R R11
U 1 1 588D568D
P 7800 2800
F 0 "R11" V 7850 3000 50  0000 C CNN
F 1 "20k" V 7800 2800 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V -170 550 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/351/Xicon_09142016_291_293_299_CF-RC_series-1002354.pdf" H -100 550 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Xicon/299-20K-RC/?qs=sGAEpiMZZMu61qfTUdNhG3dwkzs8iXRmPBZRXkWwSxg%3d" V 7800 2800 60  0001 C CNN "Order Link"
	1    7800 2800
	0    1    1    0   
$EndComp
$Comp
L R R12
U 1 1 588D56CC
P 7400 2900
F 0 "R12" V 7450 3100 50  0000 C CNN
F 1 "20k" V 7400 2900 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V -570 650 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/351/Xicon_09142016_291_293_299_CF-RC_series-1002354.pdf" H -500 650 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Xicon/299-20K-RC/?qs=sGAEpiMZZMu61qfTUdNhG3dwkzs8iXRmPBZRXkWwSxg%3d" V 7400 2900 60  0001 C CNN "Order Link"
	1    7400 2900
	0    1    1    0   
$EndComp
$Comp
L R R13
U 1 1 588D56FC
P 7000 3000
F 0 "R13" V 7050 3200 50  0000 C CNN
F 1 "20k" V 7000 3000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V -970 750 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/351/Xicon_09142016_291_293_299_CF-RC_series-1002354.pdf" H -900 750 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Xicon/299-20K-RC/?qs=sGAEpiMZZMu61qfTUdNhG3dwkzs8iXRmPBZRXkWwSxg%3d" V 7000 3000 60  0001 C CNN "Order Link"
	1    7000 3000
	0    1    1    0   
$EndComp
Wire Wire Line
	1950 2700 2250 2700
Wire Wire Line
	1950 2050 1950 2700
Wire Wire Line
	2250 2400 1950 2400
Connection ~ 1950 2400
Wire Wire Line
	2250 3900 1950 3900
Wire Wire Line
	1950 3900 1950 4600
Wire Wire Line
	2250 4200 1950 4200
Connection ~ 1950 4200
Wire Wire Line
	6150 2400 4750 2400
Wire Wire Line
	6150 2600 4750 2600
Wire Wire Line
	6150 3500 4750 3500
Wire Wire Line
	4750 3700 6150 3700
Wire Wire Line
	4950 2500 4750 2500
Wire Wire Line
	4950 3100 4750 3100
Wire Wire Line
	4750 4100 4950 4100
Wire Wire Line
	8550 2700 8350 2700
Wire Wire Line
	8550 2800 7950 2800
Wire Wire Line
	8550 2900 7550 2900
Wire Wire Line
	8550 3000 7150 3000
Wire Wire Line
	4750 2700 8050 2700
Wire Wire Line
	4750 2800 7650 2800
Wire Wire Line
	4750 2900 7250 2900
Wire Wire Line
	4750 3000 6850 3000
Wire Wire Line
	8000 2500 8000 2700
Connection ~ 8000 2700
Wire Wire Line
	7600 2800 7600 2500
Connection ~ 7600 2800
Wire Wire Line
	7200 2900 7200 2500
Connection ~ 7200 2900
Wire Wire Line
	6800 3000 6800 2500
Connection ~ 6800 3000
$Comp
L D D5
U 1 1 588D6123
P 6800 2350
F 0 "D5" V 6800 2450 50  0000 L CNN
F 1 "1N4148" V 6900 2400 50  0000 L CNN
F 2 "Diodes_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H -500 650 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/149/1N4148-888354.pdf" H -500 650 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Fairchild-Semiconductor/1N4148/?qs=sGAEpiMZZMtoHjESLttvkoBU6cp6%252bZs9oaGbBohzXiI%3d" V 6800 2350 60  0001 C CNN "Order Link"
	1    6800 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	6800 2200 6800 2000
Wire Wire Line
	6800 2000 8000 2000
Wire Wire Line
	7200 2000 7200 2200
Wire Wire Line
	7600 2000 7600 2200
Connection ~ 7200 2000
Wire Wire Line
	8000 1800 8000 2200
Connection ~ 7600 2000
Connection ~ 8000 2000
$Comp
L +3.3V #PWR014
U 1 1 588D64EF
P 8000 1800
F 0 "#PWR014" H 4700 -1800 50  0001 C CNN
F 1 "+3.3V" H 8015 1973 50  0000 C CNN
F 2 "" H 4700 -1650 50  0001 C CNN
F 3 "" H 4700 -1650 50  0001 C CNN
	1    8000 1800
	1    0    0    -1  
$EndComp
Text Label 6150 2400 0    60   ~ 0
SDA1
Text Label 6150 2600 0    60   ~ 0
SCL1
Text Label 6150 3500 0    60   ~ 0
SDA0
Text Label 6150 3700 0    60   ~ 0
SCL0
$Comp
L +3.3V #PWR015
U 1 1 588D982A
P 7050 4000
F 0 "#PWR015" H 3750 400 50  0001 C CNN
F 1 "+3.3V" H 7065 4173 50  0000 C CNN
F 2 "" H 3750 550 50  0001 C CNN
F 3 "" H 3750 550 50  0001 C CNN
	1    7050 4000
	1    0    0    -1  
$EndComp
$Comp
L R R14
U 1 1 588D9997
P 6700 4200
F 0 "R14" V 6600 4200 50  0000 C CNN
F 1 "1k" V 6700 4200 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 180 -100 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/418/NG_DS_1773195_E-1061858.pdf" H 250 -100 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/TE-Connectivity-Neohm/2-1623927-3/?qs=sGAEpiMZZMsPqMdJzcrNwhrYrPMk23ILUuNbZmXX5Ts%3d" V 6700 4200 60  0001 C CNN "Order Link"
	1    6700 4200
	0    1    1    0   
$EndComp
Wire Wire Line
	6850 4200 7050 4200
Wire Wire Line
	7050 4200 7050 4000
Wire Wire Line
	4750 4200 6550 4200
Wire Wire Line
	6350 4200 6350 4800
Connection ~ 6350 4200
$Comp
L SPDT S1
U 1 1 588D9F61
P 6900 4800
F 0 "S1" H 6900 4500 60  0000 C CNN
F 1 "SPDT" H 6900 4606 60  0000 C CNN
F 2 "LID:JS102011" H 700 -500 60  0001 C CNN
F 3 "http://www.mouser.com/ds/2/60/js-947373.pdf" H 700 -500 60  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/CK-Components/JS102011SAQN/?qs=sGAEpiMZZMtHXLepoqNyVVoF19TbJQEDLxZz69OLcdE%3d" H 6900 4800 60  0001 C CNN "Order Link"
	1    6900 4800
	-1   0    0    1   
$EndComp
Wire Wire Line
	6350 4800 6550 4800
NoConn ~ 7250 4900
Wire Wire Line
	7250 4700 8550 4700
Text HLabel 8550 4700 2    60   Input ~ 0
RESET
Wire Wire Line
	4750 3600 8550 3600
Text HLabel 8550 3600 2    60   Output ~ 0
MISO
Text HLabel 4950 3800 2    60   Output ~ 0
OUT1
Text HLabel 4950 3900 2    60   Output ~ 0
OUT2
Text HLabel 4950 4000 2    60   Output ~ 0
OUT3
Wire Wire Line
	4750 3800 4950 3800
Wire Wire Line
	4750 3900 4950 3900
Wire Wire Line
	4750 4000 4950 4000
Wire Wire Line
	8550 5700 8350 5700
Wire Wire Line
	8350 5600 8350 5800
Wire Wire Line
	8350 5600 6600 5600
Wire Wire Line
	8350 5800 6600 5800
Connection ~ 8350 5700
Wire Wire Line
	8550 6100 8350 6100
Wire Wire Line
	8350 6000 8350 6200
Wire Wire Line
	8350 6000 6600 6000
Wire Wire Line
	8350 6200 6600 6200
Connection ~ 8350 6100
Text Label 6600 5600 2    60   ~ 0
SDA0
Text Label 6600 5800 2    60   ~ 0
SCL1
Text Label 6600 6000 2    60   ~ 0
SCL0
Text Label 6600 6200 2    60   ~ 0
SDA1
$Comp
L D D6
U 1 1 58903EEE
P 7200 2350
F 0 "D6" V 7200 2450 50  0000 L CNN
F 1 "1N4148" V 7300 2400 50  0000 L CNN
F 2 "Diodes_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H -100 650 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/149/1N4148-888354.pdf" H -100 650 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Fairchild-Semiconductor/1N4148/?qs=sGAEpiMZZMtoHjESLttvkoBU6cp6%252bZs9oaGbBohzXiI%3d" V 7200 2350 60  0001 C CNN "Order Link"
	1    7200 2350
	0    1    1    0   
$EndComp
$Comp
L D D7
U 1 1 58903F25
P 7600 2350
F 0 "D7" V 7600 2450 50  0000 L CNN
F 1 "1N4148" V 7700 2400 50  0000 L CNN
F 2 "Diodes_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 300 650 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/149/1N4148-888354.pdf" H 300 650 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Fairchild-Semiconductor/1N4148/?qs=sGAEpiMZZMtoHjESLttvkoBU6cp6%252bZs9oaGbBohzXiI%3d" V 7600 2350 60  0001 C CNN "Order Link"
	1    7600 2350
	0    1    1    0   
$EndComp
$Comp
L D D8
U 1 1 58903F5D
P 8000 2350
F 0 "D8" V 8000 2450 50  0000 L CNN
F 1 "1N4148" V 8100 2400 50  0000 L CNN
F 2 "Diodes_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 700 650 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/149/1N4148-888354.pdf" H 700 650 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Fairchild-Semiconductor/1N4148/?qs=sGAEpiMZZMtoHjESLttvkoBU6cp6%252bZs9oaGbBohzXiI%3d" V 8000 2350 60  0001 C CNN "Order Link"
	1    8000 2350
	0    1    1    0   
$EndComp
$EndSCHEMATC
