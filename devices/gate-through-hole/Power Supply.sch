EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:LID
LIBS:gate-through-hole-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 2 3
Title "Logic LID Device"
Date "2017-05-03"
Rev "1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 8100 4150 2    60   Output ~ 0
+3.3V
$Comp
L VREG U1
U 1 1 588C6AFB
P 6700 4150
F 0 "U1" H 6450 3850 60  0000 C CNN
F 1 "LD1117V33" H 6700 4300 60  0000 C CNN
F 2 "TO_SOT_Packages_THT:TO-220_Neutral123_Vertical" H -50 2500 60  0001 C CNN
F 3 "http://www.mouser.com/ds/2/389/ld1117-974075.pdf" H -50 2500 60  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/STMicroelectronics/LD1117V33/?qs=sGAEpiMZZMsGz1a6aV8DcCz10aAT%252bcfyfixmM%2fM6hTA%3d" H 6700 4150 60  0001 C CNN "Order Link"
	1    6700 4150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 588C6F97
P 6700 5350
F 0 "#PWR011" H 1150 1650 50  0001 C CNN
F 1 "GND" H 6705 5177 50  0000 C CNN
F 2 "" H 1150 1900 50  0001 C CNN
F 3 "" H 1150 1900 50  0001 C CNN
	1    6700 5350
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 588C7099
P 5900 4700
F 0 "C1" H 6015 4746 50  0000 L CNN
F 1 "0.1uF" H 6015 4655 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D5.0mm_W2.5mm_P2.50mm" H -462 1200 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/427/mkseries-205465.pdf" H -500 1350 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Vishay-BC-Components/K104K15X7RF53L2/?qs=sGAEpiMZZMt3KoXD5rJ2N0gbWXNUnftXnrj%252bHeSTjwA%3d" H 5900 4700 60  0001 C CNN "Order Link"
	1    5900 4700
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 588C70E1
P 7500 4700
F 0 "C2" H 7615 4746 50  0000 L CNN
F 1 "10uF" H 7615 4655 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.00mm" H 1138 1200 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/231/lelon_REA[1]-341020.pdf" H 1100 1350 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Lelon/REA100M1HBK-0511P/?qs=sGAEpiMZZMvwFf0viD3Y3fqH3L9WtgZD%252boHqgeGyiqk%3d" H 7500 4700 60  0001 C CNN "Order Link"
	1    7500 4700
	1    0    0    -1  
$EndComp
Text HLabel 3450 4150 0    60   Input ~ 0
VA
Text HLabel 3450 4500 0    60   Input ~ 0
VB
Text HLabel 3450 4850 0    60   Input ~ 0
VC
Text HLabel 3450 5200 0    60   Input ~ 0
VD
$Comp
L D_Schottky D1
U 1 1 588C7996
P 4150 4150
F 0 "D1" H 4150 3934 50  0000 C CNN
F 1 "1N5817" H 4150 4025 50  0000 C CNN
F 2 "Diodes_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 50  2250 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/149/1N5817-888398.pdf" H 50  2250 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Fairchild-Semiconductor/1N5817/?qs=sGAEpiMZZMtQ8nqTKtFS%2fCJFZUIIOyzjWJhH2RQmKoY%3d" H 4150 4150 60  0001 C CNN "Order Link"
	1    4150 4150
	-1   0    0    1   
$EndComp
$Comp
L D_Schottky D2
U 1 1 588C8355
P 4150 4500
F 0 "D2" H 4150 4284 50  0000 C CNN
F 1 "1N5817" H 4150 4375 50  0000 C CNN
F 2 "Diodes_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 50  2600 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/149/1N5817-888398.pdf" H 50  2600 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Fairchild-Semiconductor/1N5817/?qs=sGAEpiMZZMtQ8nqTKtFS%2fCJFZUIIOyzjWJhH2RQmKoY%3d" H 4150 4500 60  0001 C CNN "Order Link"
	1    4150 4500
	-1   0    0    1   
$EndComp
$Comp
L D_Schottky D3
U 1 1 588C8380
P 4150 4850
F 0 "D3" H 4150 4634 50  0000 C CNN
F 1 "1N5817" H 4150 4725 50  0000 C CNN
F 2 "Diodes_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 50  2950 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/149/1N5817-888398.pdf" H 50  2950 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Fairchild-Semiconductor/1N5817/?qs=sGAEpiMZZMtQ8nqTKtFS%2fCJFZUIIOyzjWJhH2RQmKoY%3d" H 4150 4850 60  0001 C CNN "Order Link"
	1    4150 4850
	-1   0    0    1   
$EndComp
$Comp
L D_Schottky D4
U 1 1 588C83AC
P 4150 5200
F 0 "D4" H 4150 4984 50  0000 C CNN
F 1 "1N5817" H 4150 5075 50  0000 C CNN
F 2 "Diodes_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 50  3300 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/149/1N5817-888398.pdf" H 50  3300 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Fairchild-Semiconductor/1N5817/?qs=sGAEpiMZZMtQ8nqTKtFS%2fCJFZUIIOyzjWJhH2RQmKoY%3d" H 4150 5200 60  0001 C CNN "Order Link"
	1    4150 5200
	-1   0    0    1   
$EndComp
Wire Wire Line
	4300 4150 6200 4150
Wire Wire Line
	7200 4150 8100 4150
Wire Wire Line
	6700 4600 6700 5350
Wire Wire Line
	7500 5200 7500 4850
Wire Wire Line
	5900 5200 7500 5200
Connection ~ 6700 5200
Wire Wire Line
	5900 4850 5900 5200
Wire Wire Line
	5900 4550 5900 4150
Connection ~ 5900 4150
Wire Wire Line
	7500 4550 7500 4150
Connection ~ 7500 4150
Wire Wire Line
	3450 4150 4000 4150
Wire Wire Line
	3450 4500 4000 4500
Wire Wire Line
	3450 4850 4000 4850
Wire Wire Line
	3450 5200 4000 5200
Wire Wire Line
	4300 4500 4900 4500
Wire Wire Line
	4900 3450 4900 5200
Connection ~ 4900 4150
Wire Wire Line
	4900 4850 4300 4850
Connection ~ 4900 4500
Wire Wire Line
	4900 5200 4300 5200
Connection ~ 4900 4850
Text HLabel 8100 3450 2    60   Output ~ 0
+Vpp
Wire Wire Line
	4900 3450 8100 3450
$EndSCHEMATC
