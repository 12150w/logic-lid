EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:LID
LIBS:gate-through-hole-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 3
Title "Logic LID Device"
Date "2017-05-03"
Rev "1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GND #PWR01
U 1 1 5844B378
P 4700 3350
F 0 "#PWR01" H 4700 3100 50  0001 C CNN
F 1 "GND" H 4700 3200 50  0000 C CNN
F 2 "" H 4700 3350 50  0000 C CNN
F 3 "" H 4700 3350 50  0000 C CNN
	1    4700 3350
	1    0    0    -1  
$EndComp
$Comp
L DEVICE_SOCKET N1
U 1 1 5844B388
P 4700 2650
F 0 "N1" H 5000 2200 60  0000 R CNN
F 1 "DEVICE_SOCKET" H 4700 3000 60  0000 C CNN
F 2 "LID:LID_NODE_CONN" H 4700 3000 60  0001 C CNN
F 3 "http://www.mouser.com/ds/2/1/TS2187-335945.pdf" H 4700 3000 60  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/3M-Electronic-Solutions-Division/960103-6202-AR/?qs=sGAEpiMZZMs%252bGHln7q6pm53vbKor1bMJy7j%2fhOA1OFE%3d" H 4700 2650 60  0001 C CNN "Order Link"
F 5 "3" H 4700 2650 60  0001 C CNN "Order Quantity"
	1    4700 2650
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 5844B38A
P 1750 5400
F 0 "R6" V 1830 5400 50  0000 C CNN
F 1 "2k" V 1750 5400 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 1680 5400 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/Yageo%20LR_CFR_2013.pdf" H 1750 5400 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Yageo/CFR-25JR-52-2K/?qs=sGAEpiMZZMtlubZbdhIBICXBKsnvdtuN3EgjpBSer0E%3d" H 1750 5400 60  0001 C CNN "Order Link"
	1    1750 5400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5844B38B
P 2250 5950
F 0 "#PWR02" H 2250 5700 50  0001 C CNN
F 1 "GND" H 2250 5800 50  0000 C CNN
F 2 "" H 2250 5950 50  0000 C CNN
F 3 "" H 2250 5950 50  0000 C CNN
	1    2250 5950
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR03
U 1 1 5844B38C
P 1750 3850
F 0 "#PWR03" H 1750 3700 50  0001 C CNN
F 1 "+3.3V" H 1750 3990 50  0000 C CNN
F 2 "" H 1750 3850 50  0000 C CNN
F 3 "" H 1750 3850 50  0000 C CNN
	1    1750 3850
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5844B397
P 8300 3350
F 0 "R1" V 8380 3350 50  0000 C CNN
F 1 "100" V 8300 3350 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8230 3350 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/351/Xicon_09142016_291_293_299_CF-RC_series-1002354.pdf" H 8300 3350 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Xicon/299-100-RC/?qs=sGAEpiMZZMu61qfTUdNhG8JxbPmjYh7jzebQmAnMZ3Y%3d" V 8300 3350 60  0001 C CNN "Order Link"
	1    8300 3350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 5844B398
P 8300 4500
F 0 "#PWR04" H 8300 4250 50  0001 C CNN
F 1 "GND" H 8300 4350 50  0000 C CNN
F 2 "" H 8300 4500 50  0000 C CNN
F 3 "" H 8300 4500 50  0000 C CNN
	1    8300 4500
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 5844B3A4
P 7750 4200
F 0 "R4" V 7830 4200 50  0000 C CNN
F 1 "10k" V 7750 4200 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 7680 4200 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/351/Xicon_09142016_291_293_299_CF-RC_series-1002354.pdf" H 7750 4200 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Xicon/299-10K-RC/?qs=sGAEpiMZZMtlubZbdhIBII%252bhhPYu75Hok40rFUWzE6Y%3d" V 7750 4200 60  0001 C CNN "Order Link"
	1    7750 4200
	0    1    1    0   
$EndComp
$Comp
L Q_NPN_EBC Q1
U 1 1 587C16F2
P 8200 4200
F 0 "Q1" H 8400 4250 50  0000 L CNN
F 1 "2N3904" H 8400 4150 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 8400 4300 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/149/2N3904-888417.pdf" H 8200 4200 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Fairchild-Semiconductor/2N3904BU/?qs=sGAEpiMZZMshyDBzk1%2fWi4G1GLBZKHK10QNLT596yR0%3d" H 8200 4200 60  0001 C CNN "Order Link"
	1    8200 4200
	1    0    0    -1  
$EndComp
$Comp
L LED LED1
U 1 1 588AAE26
P 8300 3750
F 0 "LED1" V 8338 3632 50  0000 R CNN
F 1 "OUT" V 8247 3632 50  0000 R CNN
F 2 "LEDs:LED_D5.0mm" H 600 -200 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/90/cree.C503B%20BAS%20BAN%20BCS%20BCN%20GAS%20GAN%20GCS%20GCN%201094[1]-310350.pdf" H 600 -200 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Cree-Inc/C503B-BAN-CY0C0461/?qs=sGAEpiMZZMtmwHDZQCdlqaMeoG0BP2Kz8n5Xt%2f7pE7c%3d" V 8300 3750 60  0001 C CNN "Order Link"
	1    8300 3750
	0    -1   -1   0   
$EndComp
$Sheet
S 6450 2350 1000 500 
U 588C5ABE
F0 "Power Supply" 60
F1 "Power Supply.sch" 60
F2 "+3.3V" O R 7450 2450 60 
F3 "VA" I L 6450 2450 60 
F4 "VB" I L 6450 2550 60 
F5 "VC" I L 6450 2650 60 
F6 "VD" I L 6450 2750 60 
F7 "+Vpp" O R 7450 2550 60 
$EndSheet
$Sheet
S 4150 4100 1050 1000
U 588D0730
F0 "MCU" 60
F1 "MCU.sch" 60
F2 "SDA" B L 4150 4400 60 
F3 "SCL" B L 4150 4500 60 
F4 "IN3" I L 4150 5000 60 
F5 "IN2" I L 4150 4900 60 
F6 "IN1" I L 4150 4800 60 
F7 "RA" I R 5200 4200 60 
F8 "RB" I R 5200 4300 60 
F9 "RC" I R 5200 4400 60 
F10 "RD" I R 5200 4500 60 
F11 "RESET" I L 4150 4200 60 
F12 "MISO" O L 4150 4300 60 
F13 "OUT1" O R 5200 4800 60 
F14 "OUT2" O R 5200 4900 60 
F15 "OUT3" O R 5200 5000 60 
$EndSheet
$Comp
L +3.3V #PWR05
U 1 1 588D3BE3
P 7650 2250
F 0 "#PWR05" H 50  1150 50  0001 C CNN
F 1 "+3.3V" H 7665 2423 50  0000 C CNN
F 2 "" H 50  1300 50  0001 C CNN
F 3 "" H 50  1300 50  0001 C CNN
	1    7650 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3350 4700 3250
Wire Wire Line
	1750 4650 1750 5250
Wire Wire Line
	1750 3850 1750 4250
Wire Wire Line
	8300 3500 8300 3600
Wire Wire Line
	8300 3900 8300 4000
Wire Wire Line
	5200 2450 6450 2450
Wire Wire Line
	5200 2550 6450 2550
Wire Wire Line
	5200 2650 6450 2650
Wire Wire Line
	5200 2750 6450 2750
Wire Wire Line
	5200 4200 5400 4200
Wire Wire Line
	5400 4200 5400 2450
Connection ~ 5400 2450
Wire Wire Line
	5500 2550 5500 4300
Wire Wire Line
	5500 4300 5200 4300
Connection ~ 5500 2550
Wire Wire Line
	5200 4400 5600 4400
Wire Wire Line
	5600 4400 5600 2650
Connection ~ 5600 2650
Wire Wire Line
	5700 2750 5700 4500
Wire Wire Line
	5700 4500 5200 4500
Connection ~ 5700 2750
Wire Wire Line
	4150 4200 3950 4200
Wire Wire Line
	3950 4200 3950 2750
Wire Wire Line
	3950 2750 4200 2750
Wire Wire Line
	4150 4300 3850 4300
Wire Wire Line
	3850 4300 3850 2650
Wire Wire Line
	3850 2650 4200 2650
Wire Wire Line
	4200 2550 3750 2550
Wire Wire Line
	3750 2550 3750 4400
Wire Wire Line
	3750 4400 4150 4400
Wire Wire Line
	4150 4500 3650 4500
Wire Wire Line
	3650 4500 3650 2450
Wire Wire Line
	3650 2450 4200 2450
$Comp
L Q_Photo_NPN Q2
U 1 1 588E86CB
P 1650 4450
F 0 "Q2" H 1500 4300 50  0000 L CNN
F 1 "SFH 3310" V 1900 4250 50  0000 L CNN
F 2 "LEDs:LED_D3.0mm" H -1500 2100 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/311/SFH%203310,%20Lead%20(Pb)%20Free%20Product%20-%20RoHS%20Compliant-318903.pdf" H -1700 2000 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Osram-Opto-Semiconductor/SFH-3310/?qs=%2fha2pyFadujkyKAIE0XZjAbYUWEjhH%2fhfGlTObnVaho%3d" H 1650 4450 60  0001 C CNN "Order Link"
	1    1650 4450
	1    0    0    -1  
$EndComp
$Comp
L R R7
U 1 1 588EAB11
P 2250 5400
F 0 "R7" V 2330 5400 50  0000 C CNN
F 1 "2k" V 2250 5400 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 2180 5400 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/Yageo%20LR_CFR_2013.pdf" H 2250 5400 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Yageo/CFR-25JR-52-2K/?qs=sGAEpiMZZMtlubZbdhIBICXBKsnvdtuN3EgjpBSer0E%3d" V 2250 5400 60  0001 C CNN "Order Link"
	1    2250 5400
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR06
U 1 1 588EAB17
P 2250 3850
F 0 "#PWR06" H 2250 3700 50  0001 C CNN
F 1 "+3.3V" H 2250 3990 50  0000 C CNN
F 2 "" H 2250 3850 50  0000 C CNN
F 3 "" H 2250 3850 50  0000 C CNN
	1    2250 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 4650 2250 5250
Wire Wire Line
	2250 3850 2250 4250
$Comp
L Q_Photo_NPN Q3
U 1 1 588EAB1F
P 2150 4450
F 0 "Q3" H 2000 4300 50  0000 L CNN
F 1 "SFH 3310" V 2400 4250 50  0000 L CNN
F 2 "LEDs:LED_D3.0mm" H -1000 2100 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/311/SFH%203310,%20Lead%20(Pb)%20Free%20Product%20-%20RoHS%20Compliant-318903.pdf" H -1200 2000 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Osram-Opto-Semiconductor/SFH-3310/?qs=%2fha2pyFadujkyKAIE0XZjAbYUWEjhH%2fhfGlTObnVaho%3d" H 2150 4450 60  0001 C CNN "Order Link"
	1    2150 4450
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 588EAC5C
P 2750 5400
F 0 "R8" V 2830 5400 50  0000 C CNN
F 1 "2k" V 2750 5400 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 2680 5400 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/Yageo%20LR_CFR_2013.pdf" H 2750 5400 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Yageo/CFR-25JR-52-2K/?qs=sGAEpiMZZMtlubZbdhIBICXBKsnvdtuN3EgjpBSer0E%3d" V 2750 5400 60  0001 C CNN "Order Link"
	1    2750 5400
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR07
U 1 1 588EAC62
P 2750 3850
F 0 "#PWR07" H 2750 3700 50  0001 C CNN
F 1 "+3.3V" H 2750 3990 50  0000 C CNN
F 2 "" H 2750 3850 50  0000 C CNN
F 3 "" H 2750 3850 50  0000 C CNN
	1    2750 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 4650 2750 5250
Wire Wire Line
	2750 3850 2750 4250
$Comp
L Q_Photo_NPN Q4
U 1 1 588EAC6A
P 2650 4450
F 0 "Q4" H 2500 4300 50  0000 L CNN
F 1 "SFH 3310" V 2900 4250 50  0000 L CNN
F 2 "LEDs:LED_D3.0mm" H -500 2100 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/311/SFH%203310,%20Lead%20(Pb)%20Free%20Product%20-%20RoHS%20Compliant-318903.pdf" H -700 2000 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Osram-Opto-Semiconductor/SFH-3310/?qs=%2fha2pyFadujkyKAIE0XZjAbYUWEjhH%2fhfGlTObnVaho%3d" H 2650 4450 60  0001 C CNN "Order Link"
	1    2650 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 5550 1750 5750
Wire Wire Line
	1750 5750 2750 5750
Wire Wire Line
	2250 5550 2250 5950
Wire Wire Line
	2750 5750 2750 5550
Connection ~ 2250 5750
Wire Wire Line
	4150 4800 1750 4800
Connection ~ 1750 4800
Wire Wire Line
	4150 4900 2250 4900
Connection ~ 2250 4900
Wire Wire Line
	4150 5000 2750 5000
Connection ~ 2750 5000
$Comp
L VPP #PWR08
U 1 1 588D3C5B
P 9500 2250
F 0 "#PWR08" H 2750 1050 50  0001 C CNN
F 1 "VPP" H 9515 2423 50  0000 C CNN
F 2 "" H 2750 1200 50  0001 C CNN
F 3 "" H 2750 1200 50  0001 C CNN
	1    9500 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 2450 7650 2450
Wire Wire Line
	7650 2450 7650 2250
Wire Wire Line
	8000 4200 7900 4200
Wire Wire Line
	7600 4200 7400 4200
Wire Wire Line
	7400 4200 7400 4800
Wire Wire Line
	7400 4800 5200 4800
$Comp
L R R5
U 1 1 588F951C
P 7750 4900
F 0 "R5" V 7830 4900 50  0000 C CNN
F 1 "10k" V 7750 4900 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 7680 4900 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/351/Xicon_09142016_291_293_299_CF-RC_series-1002354.pdf" H 7750 4900 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Xicon/299-10K-RC/?qs=sGAEpiMZZMtlubZbdhIBII%252bhhPYu75Hok40rFUWzE6Y%3d" V 7750 4900 60  0001 C CNN "Order Link"
	1    7750 4900
	0    1    1    0   
$EndComp
Wire Wire Line
	7600 4900 5200 4900
Wire Wire Line
	7900 4900 8500 4900
$Comp
L LED LED2
U 1 1 588F9C4F
P 8800 3750
F 0 "LED2" V 8838 3632 50  0000 R CNN
F 1 "OUT" V 8747 3632 50  0000 R CNN
F 2 "LEDs:LED_D5.0mm" H 1100 -200 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/90/cree.C503B%20BAS%20BAN%20BCS%20BCN%20GAS%20GAN%20GCS%20GCN%201094[1]-310350.pdf" H 1100 -200 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Cree-Inc/C503B-BAN-CY0C0461/?qs=sGAEpiMZZMtmwHDZQCdlqaMeoG0BP2Kz8n5Xt%2f7pE7c%3d" V 8800 3750 60  0001 C CNN "Order Link"
	1    8800 3750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8800 4700 8800 3900
Wire Wire Line
	8300 4400 8300 4500
Wire Wire Line
	8800 5100 8800 5200
$Comp
L GND #PWR09
U 1 1 588FAB9D
P 8800 5200
F 0 "#PWR09" H 8800 4950 50  0001 C CNN
F 1 "GND" H 8800 5050 50  0000 C CNN
F 2 "" H 8800 5200 50  0000 C CNN
F 3 "" H 8800 5200 50  0000 C CNN
	1    8800 5200
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 588FAE17
P 8800 3350
F 0 "R2" V 8880 3350 50  0000 C CNN
F 1 "100" V 8800 3350 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8730 3350 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/351/Xicon_09142016_291_293_299_CF-RC_series-1002354.pdf" H 8800 3350 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Xicon/299-100-RC/?qs=sGAEpiMZZMu61qfTUdNhG8JxbPmjYh7jzebQmAnMZ3Y%3d" V 8800 3350 60  0001 C CNN "Order Link"
	1    8800 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 3500 8800 3600
$Comp
L R R9
U 1 1 588FB3CF
P 7750 5600
F 0 "R9" V 7830 5600 50  0000 C CNN
F 1 "10k" V 7750 5600 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 7680 5600 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/351/Xicon_09142016_291_293_299_CF-RC_series-1002354.pdf" H 7750 5600 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Xicon/299-10K-RC/?qs=sGAEpiMZZMtlubZbdhIBII%252bhhPYu75Hok40rFUWzE6Y%3d" V 7750 5600 60  0001 C CNN "Order Link"
	1    7750 5600
	0    1    1    0   
$EndComp
Wire Wire Line
	5200 5000 7400 5000
Wire Wire Line
	7400 5000 7400 5600
Wire Wire Line
	7400 5600 7600 5600
Wire Wire Line
	7900 5600 9000 5600
Wire Wire Line
	9300 5800 9300 5900
$Comp
L GND #PWR010
U 1 1 588FC3E3
P 9300 5900
F 0 "#PWR010" H 9300 5650 50  0001 C CNN
F 1 "GND" H 9300 5750 50  0000 C CNN
F 2 "" H 9300 5900 50  0000 C CNN
F 3 "" H 9300 5900 50  0000 C CNN
	1    9300 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 5400 9300 3900
$Comp
L LED LED3
U 1 1 588FC745
P 9300 3750
F 0 "LED3" V 9338 3632 50  0000 R CNN
F 1 "OUT" V 9247 3632 50  0000 R CNN
F 2 "LEDs:LED_D5.0mm" H 1600 -200 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/90/cree.C503B%20BAS%20BAN%20BCS%20BCN%20GAS%20GAN%20GCS%20GCN%201094[1]-310350.pdf" H 1600 -200 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Cree-Inc/C503B-BAN-CY0C0461/?qs=sGAEpiMZZMtmwHDZQCdlqaMeoG0BP2Kz8n5Xt%2f7pE7c%3d" V 9300 3750 60  0001 C CNN "Order Link"
	1    9300 3750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7450 2550 9500 2550
Wire Wire Line
	9500 2550 9500 2250
$Comp
L R R3
U 1 1 588FD142
P 9300 3350
F 0 "R3" V 9380 3350 50  0000 C CNN
F 1 "100" V 9300 3350 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 9230 3350 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/351/Xicon_09142016_291_293_299_CF-RC_series-1002354.pdf" H 9300 3350 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Xicon/299-100-RC/?qs=sGAEpiMZZMu61qfTUdNhG8JxbPmjYh7jzebQmAnMZ3Y%3d" V 9300 3350 60  0001 C CNN "Order Link"
	1    9300 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 3500 9300 3600
Wire Wire Line
	9300 3200 9300 2550
Connection ~ 9300 2550
Wire Wire Line
	8800 3200 8800 2550
Connection ~ 8800 2550
Wire Wire Line
	8300 3200 8300 2550
Connection ~ 8300 2550
$Comp
L Q_NPN_EBC Q5
U 1 1 58902BBF
P 8700 4900
F 0 "Q5" H 8900 4950 50  0000 L CNN
F 1 "2N3904" H 8900 4850 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 8900 5000 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/149/2N3904-888417.pdf" H 8700 4900 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Fairchild-Semiconductor/2N3904BU/?qs=sGAEpiMZZMshyDBzk1%2fWi4G1GLBZKHK10QNLT596yR0%3d" H 8700 4900 60  0001 C CNN "Order Link"
	1    8700 4900
	1    0    0    -1  
$EndComp
$Comp
L Q_NPN_EBC Q6
U 1 1 58902D43
P 9200 5600
F 0 "Q6" H 9400 5650 50  0000 L CNN
F 1 "2N3904" H 9400 5550 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 9400 5700 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/149/2N3904-888417.pdf" H 9200 5600 50  0001 C CNN
F 4 "http://www.mouser.com/ProductDetail/Fairchild-Semiconductor/2N3904BU/?qs=sGAEpiMZZMshyDBzk1%2fWi4G1GLBZKHK10QNLT596yR0%3d" H 9200 5600 60  0001 C CNN "Order Link"
	1    9200 5600
	1    0    0    -1  
$EndComp
$EndSCHEMATC
