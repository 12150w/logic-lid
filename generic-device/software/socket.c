/*
	Socket Library
	
	Manages the socket connection ... such as reading the orientation and address of the terminal.
*/
#include "socket.h"

// The orientation
byte orientation;

// The address of the socket
byte address = 0x00;

// Shift register pins (changes depending upon orientation)
byte loadPin = 0;
byte dataPin = 0;
byte clockPin = 0;


// Delays a certain amount of time
inline static void clockDelay() {
	_delay_ms(SOCKET_CLOCK_DELAY);
}

// Sets a specified pin high or low (only call after loadPin, dataPin, and clockPin are set up).
inline static void clockHigh() { PORTA |= clockPin; }
inline static void clockLow() { PORTA &= ~(clockPin); }
inline static void loadHigh() { PORTA |= loadPin; }
inline static void loadLow() { PORTA &= ~(loadPin); }

// Reads the current state of the data pin (0 if low, greater than 0 if high).
inline static byte readData() { return PINA & dataPin; }

// Sets up the IO for the socket.
void socketSetup() {
	
	// Read the orientation from the pins
	DDRA &= ~(1<<DDA3) & ~(1<<DDA4) & ~(1<<DDA5) & ~(1<<DDA6);
	byte orientationBuf[4];
	byte readCount = 0;
	
	while(1) {
		while(1) {
			_delay_ms(250);
			orientationBuf[readCount] = (PINA & 0x78);
			readCount++;
			
			if(readCount > 3) {
				byte matchingCount = 0;
				for(byte i=1; i<4; i++) {
					if(orientationBuf[i] == orientationBuf[0]) {
						matchingCount++;
					}
				}
				
				if(matchingCount == 3) {
					orientation = orientationBuf[0];
					break;
				}
				
				readCount = 0;
			}
		}
		
		// Set up the shift register pins;
		if(orientation == POS_A || orientation == (POS_A|POS_C)) {
			dataPin = POS_C;
			clockPin = POS_D;
			loadPin = POS_B;
			break;
		} else if(orientation == POS_B || orientation == (POS_B|POS_A)) {
			dataPin = POS_A;
			clockPin = POS_C;
			loadPin = POS_D;
			break;
		} else if(orientation == POS_C || orientation == (POS_C|POS_D)) {
			dataPin = POS_D;
			clockPin = POS_B;
			loadPin = POS_A;
			break;
		} else if(orientation == POS_D || orientation == (POS_D|POS_B)) {
			dataPin = POS_B;
			clockPin = POS_A;
			loadPin = POS_C;
			break;
		}
		
	}
	
	// WARNING: THIS IS FOR TESTING ONLY
	//orientation = POS_A;
	address = LID_ADDRESS;
	return;

	// Set clock and load to outputs
	DDRA |= clockPin;
	DDRA |= loadPin;
	
	// Load the address in the shift register
	clockLow();
	loadLow();
	clockDelay();
	loadHigh();
	clockDelay();
	loadLow();
	clockHigh();
	clockDelay();


	// Shift in the address
	for(byte i=0; i<8; i++) {
		clockLow();
		clockDelay();

		if(readData()) {
			address = (address << 1) | 0x01;
		} else {
			address = address << 1;
		}

		clockHigh();
		clockDelay();
	}
	clockLow();
	clockDelay();
	clockHigh();
	clockDelay();
	
	// Leave clock low
	clockLow();
	
}

// Returns the orientation.
inline byte getOrientation() {
	return orientation;
}

// Returns the address.
inline byte getAddress() {
	return address;
}
