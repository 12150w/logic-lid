/*
	Generic Device Main
	
	Contains the firmware entry point.
*/
#include "main.h"

int main(void) {
	
	// Setups
	socketSetup();
	opticSetup();
	i2cSetup(getAddress());
	
	sei();
	while(1) { }
	
}
