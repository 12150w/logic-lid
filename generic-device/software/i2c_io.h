/*
	I2C IO Drivers
	
	This module abstracts low level control of the SDA and SCL lines.
	This is used by the i2c.h module.
*/
#ifndef __I2C_IO_H__
#define __I2C_IO_H__

// Dependencies
#include <avr/io.h>
#include "types.h"
#include "socket.h"

// Functions
void i2cIOSetup();

void dataOutput();
void dataInput();
void clockOutput();
void clockInput();

void dataHigh();
void dataLow();
void clockHigh();
void clockLow();

byte readClock();

byte getSwapPins();

#endif
