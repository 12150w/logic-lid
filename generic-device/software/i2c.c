#include "i2c.h"

// state is the current state of the I2C module (see the I2C_STATE_* constants in the header).
static byte state = I2C_STATE_INACTIVE;

// myAddr is the address of this device set during setup (NOT left shifted).
static byte myAddr;

// opAddr is the address of the current operation.
static byte opAddr;

// goToInactive moves the state of the I2C module to inactive.
static void goToInactive() {
	dataInput();
	clockHigh();
	clockOutput();
	USICR &= ~(1<<USICS1) & ~(1<<USICS0) & ~(1<<USICLK);
	state = I2C_STATE_INACTIVE;
}

// sendACK pulls SDA low, waits until SCL goes high then low again, then releases SDA.
static void sendACK() {
	clockInput();
	dataLow();
	dataOutput();
	
	while(readClock() != HIGH) {}
	while(readClock() != LOW) {}
	
	dataInput();
	dataHigh();
}

// setupNextData prepares the next data to be sent in the data register.
inline static void setupNextData() {
	switch(opAddr) {

		// Send the device type and orientation
		case I2C_OP_GETINFO:
			USIDR = getType() | (getOrientation() << 1);
			break;
		
		// Send the device IO state
		case I2C_OP_GETIO:
			USIDR = getIOState();
			break;
		
		// Send nothing since this is an invalid address
		default:
			goToInactive();
			break;

	}
}

// i2cSetup sets up the USI to operate on the I2C bus and the related interrupts.
//   The passed in address is the I2C address of this device (NOT left shifted).
//   MAKE SURE TO CALL sei() after i2cSetup so that the interrupts fire.
void i2cSetup(byte address) {
	myAddr = address;
	
	// Set up the USI for Two Wire mode
	USICR |= (1<<USISIE) | (1<<USIOIE) | (1<<USIWM1);
	
	// Setup the I2C IO, disable SDA driver, enable SCL driver but release (so that start detector holds SCL low)
	i2cIOSetup();
	if(getSwapPins()) {
		USIPP = (1<<USIPOS);
	}
	
	goToInactive();
}

// USI_START Interrupt
ISR(USI_START_vect) {
	
	// Wait for clock to go low
	while(readClock() == HIGH) {}
	
	// Set up the counter for 16 external high and low transitions
	USISR &= 0xF0;
	USICR |= (1<<USICS1);
	USICR &= ~(1<<USICS0) & ~(1<<USICLK); // USICS0 should never be set so it is omitted
	dataInput();
	
	// Move to the reading address state
	state = I2C_STATE_READ_ADDR;
		
	// Release SCL
	USISR |= (1<<USISIF);
	
}

// USI_OVF Interrupt
ISR(USI_OVF_vect) {
	byte readAddress, masterRead;
	
	// Reset counter
	USISR &= 0xF0;
	USICR &= ~(1<<USICS0) & ~(1<<USICLK) & ~(1<<USICS1);

	switch(state) {
		
		// Read in Address and RW, compare to device address and ACK or NACK.
		case I2C_STATE_READ_ADDR:
			readAddress = (USIDR & 0xFE);
			masterRead = (USIDR & 0x01);
			
			if(readAddress == myAddr) {
				debugHigh();
				sendACK();
				if(masterRead) {
					// Set up the next data.
					state = I2C_STATE_MASTER_READ;
					setupNextData();
					dataHigh();
					dataOutput();
				} else {
					// The next byte will be the address the master wants to write to, then the data.
					state = I2C_STATE_MASTER_WRITE;
				}

				// Prepare counter for 8 bits
				USICR |= (1<<USICS1);
				USICR &= ~(1<<USICS0) & ~(1<<USICLK);

			} else {
				goToInactive();
			}
			
			break;
		
		// The address has been written by the master.
		case I2C_STATE_MASTER_WRITE:
			opAddr = USIDR;
			sendACK();

			// Prepare counter for the next 8 bits
			USICR |= (1<<USICS1);
			USICR &= ~(1<<USICS0) & ~(1<<USICLK);

			break;
		
		// The data has been sent to the master, set up more data if not NACK.
		case I2C_STATE_MASTER_READ:
			dataHigh();
			dataInput();
			goToInactive();
			break;
		
		// Unknown what this data is, reset to waiting.
		default:
			goToInactive();
			break;
		
	}
	debugLow();
}
