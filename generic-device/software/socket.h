#ifndef __SOCKET_H__
#define __SOCKET_H__

// Includes
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "types.h"

// Functions
void socketSetup();
byte getOrientation();
byte getAddress();

// Orientations
// PA 7 6 5 4   3 2 1 0
//---------------------
// A: 0 0 0 0   1 0 0 0  (RA)  top      left
// B: 0 0 0 1   0 0 0 0  (RB)  top      right
// C: 0 0 1 0   0 0 0 0  (RC)  bottom   left
// D: 0 1 0 0   0 0 0 0  (RD)  bottom   right
#define POS_A 0x08
#define POS_B 0x10
#define POS_C 0x20
#define POS_D 0x40

// Timing Delays
#define SOCKET_CLOCK_DELAY 2

#endif