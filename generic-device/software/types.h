/*
	Custom Type Definitions
	
	Yep, and nothing more.
*/
#ifndef __TYPES_H__
#define __TYPES_H__

// byte: 8 bits, 2 nibbles, 1/2 a word, 1/4 a long, you get the picture...
typedef unsigned char byte;

// IO Level Constants
#define HIGH 1
#define LOW  0

#endif