/*
	Debug Interface
	
	This module allows to quick generation of debug signals.
*/
#ifndef __DEBUG_H__
#define __DEBUG_H__

// Includes
#include <avr/io.h>

// Functions
void debugSetup();
void debugHigh();
void debugLow();

#endif
