/*
	Optic Library
	
	Manages the optical aspect of the LID (that is, the LEDs and phototransistors).
	To use this library include the optic.h header.
	
	Note: this library sets up and uses the PCINT interrupt.
	
*/
#include "optic.h"

// ioState contains the current status of the device IO.
// Bit: 7   6   5   4      3   2   1   0
//      IN2 IN1            OUT D1  D2  IN3
static byte ioState = 0x00;

// Run when any of the externing inputs changes.
static void inputChanged() {
	byte in1 = PINB & (1<<PINB6);
	byte in2 = PINA & (1<<PINA7);
	byte in3 = PINA & (1<<PINA1);
	byte output = 0;
	
	#ifdef LID_TYPE_AND_GATE
	output = in1 && in3;
	#endif
	
	#ifdef LID_TYPE_OR_GATE
	output = in1 || in3;
	#endif
	
	#ifdef LID_TYPE_INVERTER
	output = !in2;
	#endif

	#ifdef LID_TYPE_SWITCH
	output = in2;
	#endif
	
	if(output) {
		PORTB |= (1<<PORTB3);
	} else {
		PORTB &= ~(1<<PORTB3);
	}

	// Store the IO state
	ioState = in1 | in2 | in3 | (output<<3);

}

// getType returns the type for this device
inline byte getType() {
	#ifdef LID_TYPE_AND_GATE
	return AND_GATE_ID;
	#endif
	#ifdef LID_TYPE_OR_GATE
	return OR_GATE_ID;
	#endif
	#ifdef LID_TYPE_INVERTER
	return INVERTER_ID;
	#endif
	#ifdef LID_TYPE_SWITCH
	return SWITCH_ID;
	#endif

	return UNDEFINED_ID;
}

// getIOState returns the current IO state of the device
inline byte getIOState() { return ioState; }

// Sets up the optic interface by enabling IO and setting up interrupts.
void opticSetup() {
	
	// Setup LED Outputs
	DDRB |= (1<<DDB3) | (1<<DDB4) | (1<<DDB5);
	
	// Setup the Phototransistor Inputs
	DDRA &= ~(1<<DDA1) & ~(1<<DDA7);
	DDRB &= ~(1<<DDB6);
	
	// Setup the input change interrupt.
	PCMSK0 = (1<<PCINT7) | (1<<PCINT1);
	PCMSK1 = (1<<PCINT14);
	GIMSK |= (1<<PCIE1);
	
	inputChanged();
}

// Pin Change Interrupt (PCINT).
ISR(PCINT_vect){
	inputChanged();
}