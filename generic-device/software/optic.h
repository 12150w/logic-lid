#ifndef __OPTIC_H__
#define __OPTIC_H__

// Includes
#include <avr/io.h>
#include <avr/interrupt.h>
#include "types.h"

// Functions
void opticSetup();
byte getType();
byte getIOState();

// Device Type Numbers
#define UNDEFINED_ID          0x00
#define AND_GATE_ID           0x01
#define OR_GATE_ID            0x02
#define INVERTER_ID           0x03
#define SWITCH_ID             0x04

#endif