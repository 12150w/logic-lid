#include "i2c_io.h"

// swapPins is true if SDA and SCL should be reversed (based on orientation).
byte swapPins = 0;

// i2cIOSetup reads in the orientation and sets up the I2C IO accordingly.
void i2cIOSetup() {
	byte orientation = getOrientation();
	if(orientation == POS_C || orientation == POS_D) {
		swapPins = 1;
	}
}

// dataOutput sets the SDA pin as an output.
inline void dataOutput() {
	if(!swapPins) {
		DDRB |= (1<<DDB0);
	} else {
		DDRA |= (1<<DDA0);
	}
}

// dataInput sets the SDA pin as an input.
inline void dataInput() {
	if(!swapPins) {
		DDRB &= ~(1<<DDB0);
	} else {
		DDRA &= ~(1<<DDA0);
	}
}

// clockOutput sets the SCL pin as an output.
inline void clockOutput() {
	if(!swapPins) {
		DDRB |= (1<<DDB2);
	} else {
		DDRA |= (1<<DDA2);
	}
}

// clockInput sets the SCL pin as an input.
inline void clockInput() {
	if(!swapPins) {
		DDRB &= ~(1<<DDB2);
	} else {
		DDRA &= ~(1<<DDA2);
	}
}

// dataHigh sets the SDA PORT pin to high.
inline void dataHigh() {
	if(!swapPins) {
		PORTB |= (1<<PORTB0);
	} else {
		PORTA |= (1<<PORTA0);
	}
}

// dataLow sets the SDA PORT pin to low.
inline void dataLow() {
	if(!swapPins) {
		PORTB &= ~(1<<PORTB0);
	} else {
		PORTA &= ~(1<<PORTA0);
	}
}

// clockHigh sets the SCL PORT pin to high.
inline void clockHigh() {
	if(!swapPins) {
		PORTB |= (1<<PORTB2);
	} else {
		PORTA |= (1<<PORTA2);
	}
}

// clockLow sets the SLC PORT pin to low.
inline void clockLow() {
	if(!swapPins) {
		PORTB &= ~(1<<PORTB2);
	} else {
		PORTA &= ~(1<<PORTA2);
	}
}

// readClock returns the current state of the SCL pin.
byte readClock() {
	if(!swapPins) {
		if(PINB & (1<<PINB2)) {
			return HIGH;
		} else {
			return LOW;
		}
	} else {
		if(PINA & (1<<PINA2)) {
			return HIGH;
		} else {
			return LOW;
		}
	}
}

// getSwapPins returns HIGH if the I2C pins are swapped, otherwise returns LOW.
inline byte getSwapPins() {
	return swapPins;
}
