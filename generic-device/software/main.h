#ifndef __MAIN_H__
#define __MAIN_H__

// Includes
#include <avr/io.h>
#include "types.h"
#include "optic.h"
#include "socket.h"
#include "i2c.h"

// States
#define RUN_MODE 1
#define BUILD_MODE 0

#endif