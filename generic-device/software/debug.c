#include "debug.h"

// debugSetup sets up the debug pin.
void debugSetup() {
	DDRB |= (1<<DDB5);
}

// debugHigh outputs high on the debug pin.
inline void debugHigh() {
	PORTB |= (1<<PORTB5);
}

// debugLow outputs low on the debug pin.
inline void debugLow() {
	PORTB &= ~(1<<PORTB5);
}