/*
	ATTiny261 I2C
	
	This module uses the Universal Serial Interface (USI) to communicate on a
	multi-master I2C bus. This means that the device may be either a master
	or a slave device and bus arbitration (communicating when the bus is busy)
	is supported.
*/
#ifndef __I2C_H__
#define __I2C_H__

// Dependencies
#include <avr/io.h>
#include <avr/interrupt.h>
#include "types.h"
#include "i2c_io.h"
#include "debug.h"
#include "optic.h"
#include "socket.h"

// Functions (see source comment for details)
void i2cSetup(byte address);
byte i2cSendBytes(byte* data, byte count);
byte i2cHasBytes();
byte i2cReadBytes(byte* to, byte count);

// I2C States
#define I2C_STATE_INACTIVE              0
#define I2C_STATE_READ_ADDR             1
#define I2C_STATE_WRITE_ADDR            2
#define I2C_STATE_MASTER_READ           5
#define I2C_STATE_MASTER_WRITE          6

// Operation Addresses
#define I2C_OP_GETINFO					1
#define I2C_OP_GETIO					2

#endif
