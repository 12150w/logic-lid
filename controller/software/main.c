#include "main.h"

// bus1 is the first bus in the system.
volatile LIDBus bus1;

// cogdata is the data in the cog stack.
volatile BusData data1;

volatile unsigned char bus1Data[10];

fdserial* pcSerial;
int pcData = -1;

void main(void) {
    bus1.minAddress = 0x02;
    bus1.maxAddress = 0x08;
    bus1.sdaPin = 15;
    bus1.sclPin = 16;
    bus1.data = bus1Data;
    bus1.dataCount = 0;
    bus1.deviceCommand = DEVICE_COMMAND_GETINFO;
    data1.bus = &bus1;
    
    cognew(_load_start_bus_run_cog, &data1.bus);
    
    pcSerial = fdserial_open(
        31, // RX
        30, // TX
        0, // UNUSED
        115200  // BAUD
    );
    
    while(1) {
        
        // Handle incoming bus data
        if(bus1.dataCount) {
            unsigned int i;
            unsigned char dataSum = 0;
            
            fdserial_txChar(pcSerial, 0x00);
            fdserial_txChar(pcSerial, bus1.dataCount);
            for(i=0; i<bus1.dataCount; i++) {
                fdserial_txChar(pcSerial, bus1.data[i]);
                dataSum += bus1.data[i];
            }
            fdserial_txChar(pcSerial, ((unsigned char)0xFF)-dataSum);
            bus1.dataCount = 0;
        }
        
        // Handle incoming pc data
        while(1) {
            pcData = fdserial_rxCheck(pcSerial);
            if(pcData < 0) break;
            
            switch(pcData) {
                case DEVICE_COMMAND_GETINFO:
                    bus1.deviceCommand = DEVICE_COMMAND_GETINFO;
                    break;
                case DEVICE_COMMAND_GETIO:
                    bus1.deviceCommand = DEVICE_COMMAND_GETIO;
                    break;
            }
            
            break;
        }
        
    }
}
