#include "bus_run.h"

// bus is the pointer to the LIDBus configuration for this cog.
static _COGMEM LIDBus* bus;

// SDA and SCL masks
static _COGMEM unsigned int sdaMask;
static _COGMEM unsigned int sclMask;

// Address Space Sizing
static _COGMEM unsigned int minAddr;
static _COGMEM unsigned int maxAddr;
static _COGMEM unsigned int currentAddr;
static _COGMEM unsigned char currentCommand;

// main is the bus main routine.
_NAKED void main(LIDBus** busPointer) {
    bus = *busPointer;
    sdaMask = 1 << bus->sdaPin;
    sclMask = 1 << bus->sclPin;
    minAddr = bus->minAddress;
    maxAddr = bus->maxAddress;

    DIRA &= ~sdaMask & ~sclMask;
    OUTA &= ~sdaMask & ~sclMask;

    while(1) {
        currentAddr = minAddr;
        currentCommand = bus->deviceCommand;

        for(currentAddr = minAddr; currentAddr <= maxAddr; currentAddr+=2) {
            sendStart();

            // Check if the device is present
            if(sendByte(currentAddr & 0xFE)) {
                sendByte(currentCommand);
                
                releaseSDA();
                releaseSCL();
                waitHalf();
                sendStart();
                
                // Read in orientation
                unsigned char deviceInfo = 0x00;
                if(sendByte(currentAddr | 0x01)) {
                    deviceInfo = readByte();
                }

                sinkSDA();
                sendStop();

                switch(currentCommand)     {

                    case DEVICE_COMMAND_GETINFO:    
                        prepareData(0, BUS_MESSAGE_PRESENT);
                        prepareData(1, (unsigned char)currentAddr);
                        prepareData(2, deviceInfo);
                        sendData(3);
                        break;

                    case DEVICE_COMMAND_GETIO:
                        prepareData(0, BUS_MESSAGE_IOSTATE);
                        prepareData(1, (unsigned char)currentAddr);
                        prepareData(2, deviceInfo);
                        sendData(3);
                        break;

                }
            } else {
                sendStop();
                prepareData(0, BUS_MESSAGE_MISSING);
                prepareData(1, (unsigned char)currentAddr);
                sendData(2);
            }
        }                    
    }
}

// sendStart sends a START condition on the bus.
//   NOTE: this assumes SDA and SCL are currently released.
_NATIVE void sendStart() {
    sinkSDA();
    waitStandard();
    sinkSCL();
    waitStandard();
}

// sendStop sends a STOP condition on the bus.
//   NOTE: this assumes SDA and SCL are currently sinked (low).
_NATIVE void sendStop() {
    releaseSCL();
    waitStandard();
    releaseSDA();
    waitStandard();
}

// sendByte sends a single byte of data over the bus without sending a START or STOP condition.
//   The return value if 1 if ACK or 0 if NACK.
//   NOTE: this assumes SCL is currently sinked (low).
_NATIVE unsigned int sendByte(unsigned int data) {

    // shift out the 8 bits of data
    unsigned int mask = 0x80;
    while(1) {    
        if(data & mask) {
            releaseSDA();
        } else {
            sinkSDA();
        }

        waitHalf();
        releaseSCL();
        waitStandard();
        sinkSCL();
        waitHalf();

        mask = mask >> 1;
        if(mask == 0) break;
    }

    // Check for ACK/NACK
    unsigned int ackState;
    releaseSDA();
    waitHalf();
    releaseSCL();
    ackState = (INA & sdaMask) == 0;
    waitStandard();
    sinkSCL();
    sinkSDA();
    waitStandard();

    return ackState;
}

// readByte clocks 8 times with SDA released and reads in the data
//   NOTE: assumes SDA and SCL are sinked (low)
_NATIVE unsigned char readByte() {
    unsigned char data = 0x00;
    unsigned int i;

    releaseSDA();
    sinkSCL();

    for(i=0; i<8; i++) {
        waitStandard();
        releaseSCL();

        data = data << 1;
        if(INA & sdaMask) data = data | 0x01;

        waitStandard();
        sinkSCL();
    }

    // Send ACK
    sinkSDA();
    waitStandard();
    releaseSCL();
    waitStandard();
    sinkSCL();
    waitStandard();

    return data;
}

// sendData sets the number of bytes to send and blocks until the data is processed.
_NATIVE void sendData(unsigned int count) {
    bus->dataCount = count;
    while(bus->dataCount) {}
}
