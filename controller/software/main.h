/*
 * Logic LID Controller
 * 
 * This is the main header for the controller software.
 */
#ifndef __MAIN_H__
#define __MAIN_H__

// Includes
#include <simpletools.h>
#include <fdserial.h>
#include <propeller.h>
#include "bus_types.h"

// External COGC Files
extern char _load_start_bus_run_cog[];
extern char _load_start_pc_data_cog[];



#endif
