/*
 * Bus Module Types
 * 
 * This file contains the type definitions for the
 * bus module (so that it may be used by the cogc
 * routine.
 * 
 */
#ifndef __BUS_TYPES_H__
#define __BUS_TYPES_H__


// Bus Message Types
#define BUS_MESSAGE_PRESENT 0x01
#define BUS_MESSAGE_MISSING 0x02
#define BUS_MESSAGE_IOSTATE 0x03


// Device Data Commands
#define DEVICE_COMMAND_GETINFO         0x01
#define DEVICE_COMMAND_GETIO           0x02


// LIDBus defines a bus with connected LID devices.
typedef struct {
    
    // minAddress is the minimum device address on the bus.
    unsigned int minAddress;
    
    // maxAddress is the maximum device address on the bus.
    unsigned int maxAddress;
    
    // sdaPin is the pin number of the SDA line.
    unsigned int sdaPin;
    
    // sclPin is the pin number of the SCL line.
    unsigned int sclPin;
    
    // dataCount is the number of bytes to send (0 means there is not data to send).
    volatile unsigned int dataCount;
    
    // deviceCommand is the command to send to the device continually.
    volatile unsigned char deviceCommand;
    
    // data is the pointer to the first element data to send.
    unsigned char* data;
    
} LIDBus;


// BusData contains the stack and data for the cog running a bus.
typedef struct {
    
    // This is the general working stack
    unsigned stack[100];
    
    // This is the pointer to the bus definition
    LIDBus* bus;
    
} BusData;

#endif
