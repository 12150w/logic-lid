/*
 * Bus Run Header File
 * 
 * This is the header file for the bus controller cog code.
 */
#ifndef __BUS_RUN_H__
#define __BUS_RUN_H__

// Includes
#include <propeller.h>
#include "bus_types.h"

// Functions
void sendStart();
void sendStop();
unsigned int sendByte(unsigned int data);
unsigned char readByte();
void sendData(unsigned int count);

// Special Cog Registers
#define COGC_CLKFREQ (*(unsigned int*)0)

// WAIT_COUNT is the number of clock ticks to wait for timing
#define WAIT_COUNT 90000
//(COGC_CLKFREQ/60000)

// SDA & SCL Macros
#define releaseSCL() DIRA &= ~sclMask
#define sinkSCL() DIRA |= sclMask
#define releaseSDA() DIRA &= ~sdaMask
#define sinkSDA() DIRA |= sdaMask

// Timing Macros
#define waitStandard() waitcnt(WAIT_COUNT + CNT)
#define waitHalf() waitcnt((WAIT_COUNT/2) + CNT)

// Data Sending Macros
#define prepareData(index, byteToSend) bus->data[index] = byteToSend

#endif
