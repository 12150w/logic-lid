EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:LID
LIBS:controller-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "Controller"
Date "2017-05-03"
Rev "1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L P8X32A U1
U 1 1 57F936B4
P 2950 2550
F 0 "U1" H 2950 1450 60  0000 C CNN
F 1 "P8X32A" H 2950 3650 60  0000 C CNN
F 2 "Housings_DIP:DIP-40_W15.24mm" H 2950 2500 60  0001 C CNN
F 3 "" H 2950 2500 60  0001 C CNN
F 4 "P8X32A" H 2950 2550 60  0001 C CNN "MPN"
F 5 "Parallax" H 2950 2550 60  0001 C CNN "Manufacturer"
	1    2950 2550
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X08 P3
U 1 1 57F951C8
P 3750 1950
F 0 "P3" H 3750 2400 50  0000 C CNN
F 1 "CONN_01X08" H 3750 2500 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x08" H 3750 1950 50  0001 C CNN
F 3 "" H 3750 1950 50  0000 C CNN
	1    3750 1950
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X08 P4
U 1 1 57F9522C
P 3750 3150
F 0 "P4" H 3750 2700 50  0000 C CNN
F 1 "CONN_01X08" H 3750 2600 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x08" H 3750 3150 50  0001 C CNN
F 3 "" H 3750 3150 50  0000 C CNN
	1    3750 3150
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X08 P2
U 1 1 57F95295
P 2150 3150
F 0 "P2" H 2150 3600 50  0000 C CNN
F 1 "CONN_01X08" H 2150 3700 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x08" H 2150 3150 50  0001 C CNN
F 3 "" H 2150 3150 50  0000 C CNN
	1    2150 3150
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X08 P1
U 1 1 57F9530E
P 2150 1950
F 0 "P1" H 2150 1500 50  0000 C CNN
F 1 "CONN_01X08" H 2150 1400 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x08" H 2150 1950 50  0001 C CNN
F 3 "" H 2150 1950 50  0000 C CNN
	1    2150 1950
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR9
U 1 1 57F961CF
P 5200 2800
F 0 "#PWR9" H 5200 2550 50  0001 C CNN
F 1 "GND" H 5200 2650 50  0000 C CNN
F 2 "" H 5200 2800 50  0000 C CNN
F 3 "" H 5200 2800 50  0000 C CNN
	1    5200 2800
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR8
U 1 1 57F961EF
P 5200 2300
F 0 "#PWR8" H 5200 2150 50  0001 C CNN
F 1 "VDD" H 5200 2450 50  0000 C CNN
F 2 "" H 5200 2300 50  0000 C CNN
F 3 "" H 5200 2300 50  0000 C CNN
	1    5200 2300
	1    0    0    -1  
$EndComp
$Comp
L Crystal_Small Y1
U 1 1 57F963B2
P 5400 2550
F 0 "Y1" H 5400 2650 50  0000 C CNN
F 1 "5MHz" H 5400 2450 50  0000 C CNN
F 2 "Crystals:Crystal_HC49-U_Vertical" H 5400 2550 50  0001 C CNN
F 3 "" H 5400 2550 50  0000 C CNN
	1    5400 2550
	0    -1   -1   0   
$EndComp
$Comp
L Jumper_NO_Small JP1
U 1 1 57F966C1
P 5700 2850
F 0 "JP1" H 5700 2930 50  0000 C CNN
F 1 "Jumper_NO_Small" H 5710 2790 50  0001 C CNN
F 2 "LID:LID_SOLDER_JUMPER" H 5700 2850 50  0001 C CNN
F 3 "" H 5700 2850 50  0000 C CNN
	1    5700 2850
	1    0    0    -1  
$EndComp
Text GLabel 6000 2850 2    60   Input ~ 0
EXT_CLOCK
$Comp
L VDD #PWR2
U 1 1 57F99022
P 950 2700
F 0 "#PWR2" H 950 2550 50  0001 C CNN
F 1 "VDD" H 950 2850 50  0000 C CNN
F 2 "" H 950 2700 50  0000 C CNN
F 3 "" H 950 2700 50  0000 C CNN
	1    950  2700
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR1
U 1 1 57F99046
P 950 2400
F 0 "#PWR1" H 950 2150 50  0001 C CNN
F 1 "GND" H 950 2250 50  0000 C CNN
F 2 "" H 950 2400 50  0000 C CNN
F 3 "" H 950 2400 50  0000 C CNN
	1    950  2400
	0    1    1    0   
$EndComp
Text GLabel 2050 2600 0    60   Input ~ 0
RESET
Text GLabel 3850 1600 2    60   Input ~ 0
RX
Text GLabel 4050 1700 2    60   Output ~ 0
TX
Text GLabel 4200 1800 2    60   BiDi ~ 0
MEM_SDA
Text GLabel 4700 1900 2    60   Output ~ 0
MEM_SCL
Wire Wire Line
	3500 1600 3850 1600
Wire Wire Line
	3500 1700 4050 1700
Wire Wire Line
	3500 1800 4200 1800
Wire Wire Line
	3500 1900 4700 1900
Wire Wire Line
	3500 2000 3850 2000
Wire Wire Line
	3500 2100 3850 2100
Wire Wire Line
	3500 2200 3850 2200
Wire Wire Line
	3500 2300 3850 2300
Wire Wire Line
	1800 1600 2400 1600
Wire Wire Line
	1500 1700 2400 1700
Wire Wire Line
	1800 1800 2400 1800
Wire Wire Line
	1500 1900 2400 1900
Wire Wire Line
	1800 2000 2400 2000
Wire Wire Line
	1500 2100 2400 2100
Wire Wire Line
	1800 2200 2400 2200
Wire Wire Line
	1500 2300 2400 2300
Wire Wire Line
	2050 2800 2400 2800
Wire Wire Line
	2050 2900 2400 2900
Wire Wire Line
	2050 3000 2400 3000
Wire Wire Line
	2050 3100 2400 3100
Wire Wire Line
	2050 3200 2400 3200
Wire Wire Line
	2050 3300 2400 3300
Wire Wire Line
	2050 3400 2400 3400
Wire Wire Line
	2050 3500 2400 3500
Wire Wire Line
	3500 3500 3850 3500
Wire Wire Line
	3500 3400 3850 3400
Wire Wire Line
	3500 3300 3850 3300
Wire Wire Line
	3500 3200 3850 3200
Wire Wire Line
	3500 3100 3850 3100
Wire Wire Line
	3500 3000 3850 3000
Wire Wire Line
	3500 2900 3850 2900
Wire Wire Line
	3500 2800 3850 2800
Wire Wire Line
	3500 2700 5200 2700
Wire Wire Line
	5200 2700 5200 2800
Wire Wire Line
	3500 2400 5200 2400
Wire Wire Line
	5200 2400 5200 2300
Wire Wire Line
	3500 2500 5250 2500
Wire Wire Line
	5250 2500 5250 2400
Wire Wire Line
	5250 2400 5400 2400
Wire Wire Line
	5400 2400 5400 2450
Wire Wire Line
	3500 2600 5250 2600
Wire Wire Line
	5250 2600 5250 2700
Wire Wire Line
	5250 2700 5400 2700
Wire Wire Line
	5400 2650 5400 2850
Wire Wire Line
	5400 2850 5600 2850
Connection ~ 5400 2700
Wire Wire Line
	5800 2850 6000 2850
Connection ~ 3550 1600
Connection ~ 3550 1700
Connection ~ 3550 1800
Connection ~ 3550 1900
Connection ~ 3550 2000
Connection ~ 3550 2100
Connection ~ 3550 2200
Connection ~ 3550 2300
Connection ~ 3550 2800
Connection ~ 3550 2900
Connection ~ 3550 3000
Connection ~ 3550 3100
Connection ~ 3550 3200
Connection ~ 3550 3300
Connection ~ 3550 3400
Connection ~ 3550 3500
Connection ~ 2350 3500
Connection ~ 2350 3400
Connection ~ 2350 3300
Connection ~ 2350 3200
Connection ~ 2350 3100
Connection ~ 2350 3000
Connection ~ 2350 2900
Connection ~ 2350 2800
Connection ~ 2350 1600
Connection ~ 2350 1700
Connection ~ 2350 1800
Connection ~ 2350 1900
Connection ~ 2350 2000
Connection ~ 2350 2100
Connection ~ 2350 2200
Connection ~ 2350 2300
Wire Wire Line
	950  2400 2400 2400
Wire Wire Line
	2400 2700 950  2700
Wire Wire Line
	2400 2500 2150 2500
Wire Wire Line
	2150 2500 2150 2400
Wire Wire Line
	2400 2600 2050 2600
$Comp
L BUS_CONN C4
U 1 1 57F9AF87
P 8700 5750
F 0 "C4" H 8550 5400 60  0000 L BNN
F 1 "BUS_CONN" H 8800 6050 60  0000 C CNN
F 2 "LID:LID_NODE_INTERCONN" H 8550 5400 60  0001 C CNN
F 3 "" H 8550 5400 60  0001 C CNN
	1    8700 5750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR17
U 1 1 57F9AF8D
P 8200 6450
F 0 "#PWR17" H 8200 6200 50  0001 C CNN
F 1 "GND" H 8200 6300 50  0000 C CNN
F 2 "" H 8200 6450 50  0000 C CNN
F 3 "" H 8200 6450 50  0000 C CNN
	1    8200 6450
	1    0    0    -1  
$EndComp
$Comp
L VPP #PWR13
U 1 1 57F9AF93
P 7000 5450
F 0 "#PWR13" H 7000 5300 50  0001 C CNN
F 1 "VPP" H 7000 5600 50  0000 C CNN
F 2 "" H 7000 5450 50  0000 C CNN
F 3 "" H 7000 5450 50  0000 C CNN
	1    7000 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 5600 8350 5600
Wire Wire Line
	7000 5600 7000 5450
Wire Wire Line
	8350 5900 8200 5900
Wire Wire Line
	8200 5900 8200 6450
Wire Wire Line
	8350 5700 8000 5700
Wire Wire Line
	8000 5700 8000 6150
Wire Wire Line
	8000 6150 7150 6150
Wire Wire Line
	8350 5800 8100 5800
Wire Wire Line
	8100 5800 8100 6300
Wire Wire Line
	8100 6300 7150 6300
Text GLabel 7150 6150 0    60   BiDi ~ 0
SDA4
Text GLabel 7150 6300 0    60   BiDi ~ 0
SCL4
$Comp
L R R8
U 1 1 57F9D83A
P 7650 5900
F 0 "R8" V 7730 5900 50  0000 C CNN
F 1 "2k" V 7650 5900 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 7580 5900 50  0001 C CNN
F 3 "" H 7650 5900 50  0000 C CNN
	1    7650 5900
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 57F9D897
P 7450 5900
F 0 "R4" V 7530 5900 50  0000 C CNN
F 1 "2k" V 7450 5900 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 7380 5900 50  0001 C CNN
F 3 "" H 7450 5900 50  0000 C CNN
	1    7450 5900
	1    0    0    -1  
$EndComp
Connection ~ 2150 2400
Wire Wire Line
	7450 5750 7450 5600
Connection ~ 7450 5600
Wire Wire Line
	7650 5750 7650 5600
Connection ~ 7650 5600
Wire Wire Line
	7450 6050 7450 6150
Connection ~ 7450 6150
Wire Wire Line
	7650 6050 7650 6300
Connection ~ 7650 6300
$Comp
L BUS_CONN C3
U 1 1 5824DF9E
P 8700 4450
F 0 "C3" H 8550 4100 60  0000 L BNN
F 1 "BUS_CONN" H 8800 4750 60  0000 C CNN
F 2 "LID:LID_NODE_INTERCONN" H 8550 4100 60  0001 C CNN
F 3 "" H 8550 4100 60  0001 C CNN
	1    8700 4450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR16
U 1 1 5824DFA4
P 8200 5150
F 0 "#PWR16" H 8200 4900 50  0001 C CNN
F 1 "GND" H 8200 5000 50  0000 C CNN
F 2 "" H 8200 5150 50  0000 C CNN
F 3 "" H 8200 5150 50  0000 C CNN
	1    8200 5150
	1    0    0    -1  
$EndComp
$Comp
L VPP #PWR12
U 1 1 5824DFAA
P 7000 4150
F 0 "#PWR12" H 7000 4000 50  0001 C CNN
F 1 "VPP" H 7000 4300 50  0000 C CNN
F 2 "" H 7000 4150 50  0000 C CNN
F 3 "" H 7000 4150 50  0000 C CNN
	1    7000 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 4300 8350 4300
Wire Wire Line
	7000 4300 7000 4150
Wire Wire Line
	8350 4600 8200 4600
Wire Wire Line
	8200 4600 8200 5150
Wire Wire Line
	8350 4400 8000 4400
Wire Wire Line
	8000 4400 8000 4850
Wire Wire Line
	8000 4850 7150 4850
Wire Wire Line
	8350 4500 8100 4500
Wire Wire Line
	8100 4500 8100 5000
Wire Wire Line
	8100 5000 7150 5000
Text GLabel 7150 4850 0    60   BiDi ~ 0
SDA3
Text GLabel 7150 5000 0    60   BiDi ~ 0
SCL3
$Comp
L R R7
U 1 1 5824DFC0
P 7650 4600
F 0 "R7" V 7730 4600 50  0000 C CNN
F 1 "2k" V 7650 4600 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 7580 4600 50  0001 C CNN
F 3 "" H 7650 4600 50  0000 C CNN
	1    7650 4600
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 5824DFC6
P 7450 4600
F 0 "R3" V 7530 4600 50  0000 C CNN
F 1 "2k" V 7450 4600 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 7380 4600 50  0001 C CNN
F 3 "" H 7450 4600 50  0000 C CNN
	1    7450 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 4450 7450 4300
Connection ~ 7450 4300
Wire Wire Line
	7650 4450 7650 4300
Connection ~ 7650 4300
Wire Wire Line
	7450 4750 7450 4850
Connection ~ 7450 4850
Wire Wire Line
	7650 4750 7650 5000
Connection ~ 7650 5000
$Comp
L BUS_CONN C2
U 1 1 5824E0DA
P 8700 3150
F 0 "C2" H 8550 2800 60  0000 L BNN
F 1 "BUS_CONN" H 8800 3450 60  0000 C CNN
F 2 "LID:LID_NODE_INTERCONN" H 8550 2800 60  0001 C CNN
F 3 "" H 8550 2800 60  0001 C CNN
	1    8700 3150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR15
U 1 1 5824E0E0
P 8200 3850
F 0 "#PWR15" H 8200 3600 50  0001 C CNN
F 1 "GND" H 8200 3700 50  0000 C CNN
F 2 "" H 8200 3850 50  0000 C CNN
F 3 "" H 8200 3850 50  0000 C CNN
	1    8200 3850
	1    0    0    -1  
$EndComp
$Comp
L VPP #PWR11
U 1 1 5824E0E6
P 7000 2850
F 0 "#PWR11" H 7000 2700 50  0001 C CNN
F 1 "VPP" H 7000 3000 50  0000 C CNN
F 2 "" H 7000 2850 50  0000 C CNN
F 3 "" H 7000 2850 50  0000 C CNN
	1    7000 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 3000 8350 3000
Wire Wire Line
	7000 3000 7000 2850
Wire Wire Line
	8350 3300 8200 3300
Wire Wire Line
	8200 3300 8200 3850
Wire Wire Line
	8350 3100 8000 3100
Wire Wire Line
	8000 3100 8000 3550
Wire Wire Line
	8000 3550 7150 3550
Wire Wire Line
	8350 3200 8100 3200
Wire Wire Line
	8100 3200 8100 3700
Wire Wire Line
	8100 3700 7150 3700
Text GLabel 7150 3550 0    60   BiDi ~ 0
SDA2
Text GLabel 7150 3700 0    60   BiDi ~ 0
SCL2
$Comp
L R R6
U 1 1 5824E0FC
P 7650 3300
F 0 "R6" V 7730 3300 50  0000 C CNN
F 1 "2k" V 7650 3300 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 7580 3300 50  0001 C CNN
F 3 "" H 7650 3300 50  0000 C CNN
	1    7650 3300
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5824E102
P 7450 3300
F 0 "R2" V 7530 3300 50  0000 C CNN
F 1 "2k" V 7450 3300 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 7380 3300 50  0001 C CNN
F 3 "" H 7450 3300 50  0000 C CNN
	1    7450 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3150 7450 3000
Connection ~ 7450 3000
Wire Wire Line
	7650 3150 7650 3000
Connection ~ 7650 3000
Wire Wire Line
	7450 3450 7450 3550
Connection ~ 7450 3550
Wire Wire Line
	7650 3450 7650 3700
Connection ~ 7650 3700
$Comp
L BUS_CONN C1
U 1 1 5824E5F8
P 8700 1850
F 0 "C1" H 8550 1500 60  0000 L BNN
F 1 "BUS_CONN" H 8800 2150 60  0000 C CNN
F 2 "LID:LID_NODE_INTERCONN" H 8550 1500 60  0001 C CNN
F 3 "" H 8550 1500 60  0001 C CNN
	1    8700 1850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR14
U 1 1 5824E5FE
P 8200 2550
F 0 "#PWR14" H 8200 2300 50  0001 C CNN
F 1 "GND" H 8200 2400 50  0000 C CNN
F 2 "" H 8200 2550 50  0000 C CNN
F 3 "" H 8200 2550 50  0000 C CNN
	1    8200 2550
	1    0    0    -1  
$EndComp
$Comp
L VPP #PWR10
U 1 1 5824E604
P 7000 1550
F 0 "#PWR10" H 7000 1400 50  0001 C CNN
F 1 "VPP" H 7000 1700 50  0000 C CNN
F 2 "" H 7000 1550 50  0000 C CNN
F 3 "" H 7000 1550 50  0000 C CNN
	1    7000 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 1700 8350 1700
Wire Wire Line
	7000 1700 7000 1550
Wire Wire Line
	8350 2000 8200 2000
Wire Wire Line
	8200 2000 8200 2550
Wire Wire Line
	8350 1800 8000 1800
Wire Wire Line
	8000 1800 8000 2250
Wire Wire Line
	8000 2250 7150 2250
Wire Wire Line
	8350 1900 8100 1900
Wire Wire Line
	8100 1900 8100 2400
Wire Wire Line
	8100 2400 7150 2400
Text GLabel 7150 2250 0    60   BiDi ~ 0
SDA1
Text GLabel 7150 2400 0    60   BiDi ~ 0
SCL1
$Comp
L R R5
U 1 1 5824E61A
P 7650 2000
F 0 "R5" V 7730 2000 50  0000 C CNN
F 1 "2k" V 7650 2000 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 7580 2000 50  0001 C CNN
F 3 "" H 7650 2000 50  0000 C CNN
	1    7650 2000
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5824E620
P 7450 2000
F 0 "R1" V 7530 2000 50  0000 C CNN
F 1 "2k" V 7450 2000 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 7380 2000 50  0001 C CNN
F 3 "" H 7450 2000 50  0000 C CNN
	1    7450 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 1850 7450 1700
Connection ~ 7450 1700
Wire Wire Line
	7650 1850 7650 1700
Connection ~ 7650 1700
Wire Wire Line
	7450 2150 7450 2250
Connection ~ 7450 2250
Wire Wire Line
	7650 2150 7650 2400
Connection ~ 7650 2400
Text GLabel 1800 1600 0    60   BiDi ~ 0
SDA1
Text GLabel 1500 1700 0    60   BiDi ~ 0
SCL1
Text GLabel 1800 1800 0    60   BiDi ~ 0
SDA2
Text GLabel 1500 1900 0    60   BiDi ~ 0
SCL2
Text GLabel 1800 2000 0    60   BiDi ~ 0
SDA3
Text GLabel 1500 2100 0    60   BiDi ~ 0
SCL3
Text GLabel 1800 2200 0    60   BiDi ~ 0
SDA4
Text GLabel 1500 2300 0    60   BiDi ~ 0
SCL4
$Comp
L 24LC256 U2
U 1 1 5845A805
P 2950 4350
F 0 "U2" H 3150 4100 60  0000 C CNN
F 1 "24LC256" H 2950 4750 60  0000 C CNN
F 2 "" H 2950 4300 60  0001 C CNN
F 3 "" H 2950 4300 60  0001 C CNN
	1    2950 4350
	1    0    0    -1  
$EndComp
Text GLabel 3850 4600 2    60   Input ~ 0
MEM_SCL
Text GLabel 3500 4150 2    60   BiDi ~ 0
MEM_SDA
Wire Wire Line
	3500 4600 3850 4600
$Comp
L VDD #PWR7
U 1 1 5845B468
P 4650 3900
F 0 "#PWR7" H 4650 3750 50  0001 C CNN
F 1 "VDD" H 4650 4050 50  0000 C CNN
F 2 "" H 4650 3900 50  0000 C CNN
F 3 "" H 4650 3900 50  0000 C CNN
	1    4650 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 4000 4650 4000
Wire Wire Line
	4650 4000 4650 3900
$Comp
L GND #PWR5
U 1 1 5845B99B
P 2350 4550
F 0 "#PWR5" H 2350 4300 50  0001 C CNN
F 1 "GND" H 2350 4400 50  0000 C CNN
F 2 "" H 2350 4550 50  0000 C CNN
F 3 "" H 2350 4550 50  0000 C CNN
	1    2350 4550
	1    0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 5845BA41
P 4150 4250
F 0 "R9" V 4230 4250 50  0000 C CNN
F 1 "10k" V 4150 4250 50  0000 C CNN
F 2 "" V 4080 4250 50  0000 C CNN
F 3 "" H 4150 4250 50  0000 C CNN
	1    4150 4250
	0    1    1    0   
$EndComp
Connection ~ 4550 4000
$Comp
L FT232R U3
U 1 1 5845D5E2
P 2950 6000
F 0 "U3" H 2950 5050 60  0000 C CNN
F 1 "FT232R" H 2950 6700 60  0000 C CNN
F 2 "" H 2700 5950 60  0001 C CNN
F 3 "" H 2700 5950 60  0001 C CNN
	1    2950 6000
	1    0    0    -1  
$EndComp
$Comp
L USB_B P5
U 1 1 5845D82E
P 1400 4300
F 0 "P5" H 1600 4100 50  0000 C CNN
F 1 "USB_B" H 1350 4500 50  0000 C CNN
F 2 "" V 1350 4200 50  0000 C CNN
F 3 "" V 1350 4200 50  0000 C CNN
	1    1400 4300
	0    -1   1    0   
$EndComp
Wire Wire Line
	1850 5100 1950 5100
$Comp
L GND #PWR3
U 1 1 5845DEA6
P 1450 5800
F 0 "#PWR3" H 1450 5550 50  0001 C CNN
F 1 "GND" H 1450 5650 50  0000 C CNN
F 2 "" H 1450 5800 50  0000 C CNN
F 3 "" H 1450 5800 50  0000 C CNN
	1    1450 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 5000 3750 5000
Wire Wire Line
	3750 5000 3750 5550
Wire Wire Line
	3750 5550 3500 5550
Wire Wire Line
	1400 4900 3850 4900
Wire Wire Line
	3850 4900 3850 5450
Wire Wire Line
	3850 5450 3500 5450
Wire Wire Line
	2050 5450 2350 5450
Wire Wire Line
	2250 4800 2250 5450
Wire Wire Line
	1900 6150 2350 6150
Wire Wire Line
	2250 6150 2250 6250
Wire Wire Line
	2250 6250 2350 6250
Wire Wire Line
	2350 6400 2250 6400
Wire Wire Line
	2250 6400 2250 6900
$Comp
L GND #PWR4
U 1 1 5845FB73
P 2250 6900
F 0 "#PWR4" H 2250 6650 50  0001 C CNN
F 1 "GND" H 2250 6750 50  0000 C CNN
F 2 "" H 2250 6900 50  0000 C CNN
F 3 "" H 2250 6900 50  0000 C CNN
	1    2250 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 6800 2350 6800
Connection ~ 2250 6800
Wire Wire Line
	2350 6700 2250 6700
Connection ~ 2250 6700
Wire Wire Line
	2350 6600 2250 6600
Connection ~ 2250 6600
Wire Wire Line
	2350 6500 2250 6500
Connection ~ 2250 6500
$Comp
L C C5
U 1 1 58460033
P 1900 6450
F 0 "C5" H 1925 6550 50  0000 L CNN
F 1 "0.1uF" H 1925 6350 50  0000 L CNN
F 2 "" H 1938 6300 50  0000 C CNN
F 3 "" H 1900 6450 50  0000 C CNN
	1    1900 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 6150 1900 6300
Connection ~ 2250 6150
Wire Wire Line
	1900 6600 1900 6800
$Comp
L R R10
U 1 1 58460607
P 4000 5700
F 0 "R10" V 3900 5700 50  0000 C CNN
F 1 "150" V 4000 5700 50  0000 C CNN
F 2 "" V 3930 5700 50  0000 C CNN
F 3 "" H 4000 5700 50  0000 C CNN
	1    4000 5700
	0    1    1    0   
$EndComp
$Comp
L R R11
U 1 1 58460694
P 4300 5800
F 0 "R11" V 4380 5800 50  0000 C CNN
F 1 "150" V 4300 5800 50  0000 C CNN
F 2 "" V 4230 5800 50  0000 C CNN
F 3 "" H 4300 5800 50  0000 C CNN
	1    4300 5800
	0    1    1    0   
$EndComp
Wire Wire Line
	3500 5700 3850 5700
Wire Wire Line
	3500 5800 4150 5800
Wire Wire Line
	4150 5700 4600 5700
Wire Wire Line
	4450 5800 4800 5800
Text GLabel 4800 5800 2    60   Output ~ 0
RX
Text GLabel 4600 5700 2    60   Input ~ 0
TX
Wire Wire Line
	1500 4800 2250 4800
Wire Wire Line
	1450 5450 1450 5800
Wire Wire Line
	1950 5100 1950 5450
Wire Wire Line
	1950 5450 1450 5450
Wire Wire Line
	2050 5450 2050 5700
Wire Wire Line
	2050 5700 1950 5700
Connection ~ 2250 5450
$Comp
L C C6
U 1 1 58462636
P 1800 5700
F 0 "C6" H 1825 5800 50  0000 L CNN
F 1 "4.6uF" H 1825 5600 50  0000 L CNN
F 2 "" H 1838 5550 50  0000 C CNN
F 3 "" H 1800 5700 50  0000 C CNN
	1    1800 5700
	0    1    1    0   
$EndComp
Wire Wire Line
	1650 5700 1450 5700
Connection ~ 1450 5700
$Comp
L C C?
U 1 1 58462EE7
P 4300 6150
F 0 "C?" H 4325 6250 50  0000 L CNN
F 1 "10nF" H 4325 6050 50  0000 L CNN
F 2 "" H 4338 6000 50  0000 C CNN
F 3 "" H 4300 6150 50  0000 C CNN
	1    4300 6150
	0    1    1    0   
$EndComp
Wire Wire Line
	3500 6150 4150 6150
Wire Wire Line
	4450 6150 5250 6150
Wire Wire Line
	5250 6150 5250 6000
Wire Wire Line
	5250 6000 5450 6000
$Comp
L Q_NPN_EBC Q?
U 1 1 58463302
P 5650 6000
F 0 "Q?" H 5950 6050 50  0000 R CNN
F 1 "Q_NPN_EBC" H 6250 5950 50  0000 R CNN
F 2 "" H 5850 6100 50  0000 C CNN
F 3 "" H 5650 6000 50  0000 C CNN
	1    5650 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 6150 4550 6400
Connection ~ 4550 6150
$Comp
L R R?
U 1 1 584634A5
P 4550 6550
F 0 "R?" V 4630 6550 50  0000 C CNN
F 1 "33k" V 4550 6550 50  0000 C CNN
F 2 "" V 4480 6550 50  0000 C CNN
F 3 "" H 4550 6550 50  0000 C CNN
	1    4550 6550
	-1   0    0    1   
$EndComp
Wire Wire Line
	4550 6700 4550 6950
$Comp
L GND #PWR?
U 1 1 58463686
P 4550 6950
F 0 "#PWR?" H 4550 6700 50  0001 C CNN
F 1 "GND" H 4550 6800 50  0000 C CNN
F 2 "" H 4550 6950 50  0000 C CNN
F 3 "" H 4550 6950 50  0000 C CNN
	1    4550 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 5800 5750 5600
Text GLabel 5750 5600 1    60   Output ~ 0
RESET
Wire Wire Line
	5750 6200 5750 6750
Wire Wire Line
	5750 6750 4550 6750
Connection ~ 4550 6750
Wire Wire Line
	1500 4800 1500 4600
Wire Wire Line
	1400 4900 1400 4600
Wire Wire Line
	1850 4200 1850 5100
Wire Wire Line
	1850 4300 1800 4300
Wire Wire Line
	1800 4200 1850 4200
Connection ~ 1850 4300
Wire Wire Line
	1200 5000 1200 4600
Wire Wire Line
	2950 4000 2950 4050
Wire Wire Line
	2350 4250 2350 4550
Wire Wire Line
	2350 4250 2550 4250
Wire Wire Line
	2550 4350 2350 4350
Connection ~ 2350 4350
Wire Wire Line
	2550 4450 2350 4450
Connection ~ 2350 4450
Wire Wire Line
	2500 4650 3350 4650
Wire Wire Line
	2500 4650 2500 4500
Wire Wire Line
	2500 4500 2350 4500
Connection ~ 2350 4500
Wire Wire Line
	3350 4650 3350 4450
Connection ~ 2950 4650
Wire Wire Line
	3350 4350 3500 4350
Wire Wire Line
	3500 4350 3500 4600
Wire Wire Line
	3350 4250 4000 4250
Wire Wire Line
	4300 4250 4550 4250
Wire Wire Line
	4550 4250 4550 4000
Wire Wire Line
	3500 4150 3450 4150
Wire Wire Line
	3450 4150 3450 4250
Connection ~ 3450 4250
$EndSCHEMATC
